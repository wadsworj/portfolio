#ifndef PARAMETER_MANAGER_H
#define PARAMETER_MANAGER_H

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>



/* 
 * 64-bit integers 
 */ 
#ifdef HAVE_LONG_INT_64 
/* Plain "long int" fits, use it */ 

#ifndef HAVE_INT64 
typedef long int int64; 
#endif 
#ifndef HAVE_UINT64 
typedef unsigned long int uint64; 
#endif 
#elif defined(HAVE_LONG_LONG_INT_64) 

/* We have working support for "long long int", use that */ 

#ifndef HAVE_INT64 
typedef long long int int64; 
#endif 
#ifndef HAVE_UINT64 
typedef unsigned long long int uint64; 
#endif 
#else
/* neither HAVE_LONG_INT_64 nor HAVE_LONG_LONG_INT_64 */ 
#error must have a working 64-bit integer datatype 
#endif 
 

typedef enum { WHITE_STATE, EQUAL_STATE, STRING_STATE, INT_STATE,
                REAL_STATE, BOOLEAN_STATE, LIST_STATE} state_t;
                
typedef enum { INT_TYPE, REAL_TYPE, BOOLEAN_TYPE, STRING_TYPE, LIST_TYPE} param_t;
                
/* Boolean Type */
typedef int Boolean;
#define true 1
#define false 0

/* ParameterList Type */
typedef struct ParameterList {
    int size;
    struct ListElement * current;
    struct ListElement **elements;
} ParameterList;
                
/* Union data type provided by assignment spec */
union param_value
{
    int           int_val;
    float         real_val;
    Boolean       bool_val;
    char          *str_val;
    ParameterList *list_val;
};


/* Parameter Type */
typedef struct Parameter {
	char* name;
    int required;
    Boolean empty;
    param_t ptype;
    union param_value value;
    struct Parameter * nextChain;
}Parameter;

typedef struct {
    int sizeOfTable;
	union param_value *values;
	Parameter ** parametersTable;
} ParameterManager;

/* Paramater Manager Functions */
ParameterManager * PM_create(int size);
int PM_destroy(ParameterManager *p);
int PM_parseFrom(ParameterManager *p, FILE *fp, char comment);
int PM_manage(ParameterManager *p, char *pname, param_t ptype, int required);
int PM_hasValue(ParameterManager *p, char *pname);
union param_value PM_getValue(ParameterManager *p, char *pname);
Boolean PM_hasRequiredParameters(ParameterManager * manager);
void PM_returnToState(ParameterManager * p);

/* Paramater List Functions */
char * PL_next(ParameterList *l);
int PL_reset(ParameterList * list);

/* ListElement */
typedef struct ListElement {
    /*change name of this */
    char *c;
    struct ListElement * next;
} ListElement;

void addListValue(ListElement ** root, char * name);
void printEntireList(ListElement ** root);

/* HashTable.h */
int HT_nextPrime(int number);
int HT_createKey(char * string, int hashSize);
Parameter * HT_findParameter(Parameter * head, char* name);
Boolean checkIfEqual(Parameter * node, char* name);
Parameter * HT_addParameter(Parameter * root, Parameter * newParameter, char* name);

/* code generating Functions */
int generateInterface(ParameterManager * manager, char * fileName, char * workingDirectory);
int generateCompiled(ParameterManager * manager, char * fileName, char * workingDirectory);

#endif
