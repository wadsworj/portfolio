/* ./example/exampleFieldEdit.java */

package example;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.BevelBorder;

/* Interface: exampleFieldEdit */
public interface exampleFieldEdit
{
	public String getDCName();
	public void setDCName(String string);

	public String getDCStudent_ID();
	public void setDCStudent_ID(String string);

	public String getDCA1();
	public void setDCA1(String string);

	public String getDCA2();
	public void setDCA2(String string);

	public String getDCA3();
	public void setDCA3(String string);

	public String getDCA4();
	public void setDCA4(String string);

	public String getDCAverage();
	public void setDCAverage(String string);

}


