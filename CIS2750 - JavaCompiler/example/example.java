/* ./example/example.java */

package example;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.BevelBorder;

public class example extends JFrame implements exampleFieldEdit
{
	public static void main(String[] args)
	{
		new example();
	}
	private JTextField Name;
	private JTextField Student_ID;
	private JTextField A1;
	private JTextField A2;
	private JTextField A3;
	private JTextField A4;
	private JTextField Average;

	public example()
	{

		setTitle("compiled_example");
		setSize(451, 520);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JPanel fieldsPanel = new JPanel();
		GridLayout grid = new GridLayout(0, 2);
		grid.setHgap(10);

		fieldsPanel.setLayout(grid);

		Name = new JTextField();
		fieldsPanel.add(new JLabel("Name"));
		fieldsPanel.add(Name);

		Student_ID = new JTextField();
		fieldsPanel.add(new JLabel("Student_ID"));
		fieldsPanel.add(Student_ID);

		A1 = new JTextField();
		fieldsPanel.add(new JLabel("A1"));
		fieldsPanel.add(A1);

		A2 = new JTextField();
		fieldsPanel.add(new JLabel("A2"));
		fieldsPanel.add(A2);

		A3 = new JTextField();
		fieldsPanel.add(new JLabel("A3"));
		fieldsPanel.add(A3);

		A4 = new JTextField();
		fieldsPanel.add(new JLabel("A4"));
		fieldsPanel.add(A4);

		Average = new JTextField();
		fieldsPanel.add(new JLabel("Average"));
		fieldsPanel.add(Average);

		panel.add(fieldsPanel, BorderLayout.NORTH);

		/* Buttons Panel */
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));
		JButton AddButton = new JButton("Add");
		buttonsPanel.add(AddButton);

		JButton UpdateButton = new JButton("Update");
		buttonsPanel.add(UpdateButton);

		JButton DeleteButton = new JButton("Delete");
		buttonsPanel.add(DeleteButton);

		JButton QueryButton = new JButton("Query");
		buttonsPanel.add(QueryButton);

		panel.add(buttonsPanel, BorderLayout.CENTER);

		JPanel statusPanel = new JPanel();
		statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		JLabel statusLabel = new JLabel("STATUS");
		JTextField statusText = new JTextField();
		statusPanel.add(statusLabel);
		panel.add(statusPanel, BorderLayout.CENTER);
		panel.add(statusText, BorderLayout.CENTER);
		add(panel);
		
		

		setVisible(true);
	}

	/* Getters and setters */
	public String getDCName()
	{
		return Name.getText();
	}
	public void setDCName(String string)
	{
		Name.setText(string);
	}
	public String getDCStudent_ID()
	{
		return Student_ID.getText();
	}
	public void setDCStudent_ID(String string)
	{
		Student_ID.setText(string);
	}
	public String getDCA1()
	{
		return A1.getText();
	}
	public void setDCA1(String string)
	{
		A1.setText(string);
	}
	public String getDCA2()
	{
		return A2.getText();
	}
	public void setDCA2(String string)
	{
		A2.setText(string);
	}
	public String getDCA3()
	{
		return A3.getText();
	}
	public void setDCA3(String string)
	{
		A3.setText(string);
	}
	public String getDCA4()
	{
		return A4.getText();
	}
	public void setDCA4(String string)
	{
		A4.setText(string);
	}
	public String getDCAverage()
	{
		return Average.getText();
	}
	public void setDCAverage(String string)
	{
		Average.setText(string);
	}
}


