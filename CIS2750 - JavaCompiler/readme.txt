****************************
1. File Information
****************************
Library Name: libJNIpm.so
Name: James Wadsworth,
User ID: 0813414


****************************
2 . To Compile + Run:
****************************
1) In main directory 'a2', enter 'make'

2) To run, enter 'make run'

We assume that the user will compile AND run this program
with the commands provided by the makefile ('make' and 'make run').
You can compile the program and run it from the command line
without using the provided makefile, but support for this is not
provided.

PLEASE USE make run TO LOAD GUI

If you choose not to, here are the file structures:

a2/		(.h and .so files here)
a2/src		(.c files here)
a2/src/cis2750_a2	(.java files here)
a2/src/generated	(generated .java and .class files here)
a2/build/cis2750_a2	(.class files here)

****************************
3. Assumptions
****************************
-If a user names multiple button/fields with the same name,
	the java file will not compile


****************************
4. Limitations:
****************************
-Parameter Names and strings do not exceed 1024 characters

-User can open multiple "Help" and "About" windows
