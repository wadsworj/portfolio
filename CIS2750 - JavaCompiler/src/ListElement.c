/*
 * 2750_A1 - Winter 2015
 * Name: James Wadsworth,
 * User ID: 0813414
*/

#include "ParameterManager.h"

/*
A function to add a list element (string) to the end of a list

void addListValue(ListElement ** root, char * name);
	Add list to end
	PRE: n/a
	POST: Returns the next item in parameter list l, NULL if there are no items remaining in the list (see note below)
*/
void addListValue(ListElement ** root, char * name)
{
	ListElement * node;
    ListElement * newNode;
	char * newString;
    
    /* create a temp ELEMENT */
    newNode = malloc(sizeof(ListElement));
    newString = malloc(sizeof(char*)*strlen(name));
	strcpy(newString, name);
    newNode->next = NULL;
    newNode->c = newString;
    
    if(*root == NULL)
    {
        *root = newNode;
        return;
    }
    node = *root;
    
    while (node->next != NULL)
        node = node->next;

    node->next = newNode;
    
}

/*
void printEntireList(ListElement ** root);
	Print entire list
	PRE: n/a
	POST: Returns the next item in parameter list l, NULL if there are no items remaining in the list (see note below)
*/
void printEntireList(ListElement ** root)
{
    ListElement * node;
    node = *root;
    while(node != NULL)
    {
        printf("String: %s\n", node->c);
        node = node->next;
    }
}
