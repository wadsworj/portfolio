/*
 * 2750_A1 - Winter 2015
 * Name: James Wadsworth,
 * User ID: 0813414
*/

#include "ParameterManager.h"

/*
ParameterManager * PM_create(int size);
	Creates a new parameter manager object
	PRE: size is a positive integer (see note 1 below)
	POST: Returns a new parameter manager object initialized to be
          empty (i.e. managing no parameters) on success, NULL
          otherwise (memory allocation failure)
*/
ParameterManager * PM_create(int size)
{
    int sizeOfHash;
    int index;
	ParameterManager * manager = NULL;
	
    /* test for valid size input */
	if (size < 1)
		return NULL;
    
    /* allocate and test space for the parameter manager */
    if ((manager = malloc(sizeof(ParameterManager))) == NULL)
        return NULL;
    
    sizeOfHash = HT_nextPrime(size);
    manager->sizeOfTable = sizeOfHash;
    
    
    /* allocate and test space for the hash table, contained within the parameter manager */
	if ((manager->parametersTable = malloc(sizeof(Parameter)*sizeOfHash)) == NULL)
        return NULL;
    
    /* each element of table is initialized to NULL */
    for (index = 0; index < sizeOfHash; index++)
        manager->parametersTable[index] = NULL;
	return manager;
}

/*
int PM_destroy(ParameterManager *p);
	Destroys a parameter manager object
	PRE: n/a
	POST: all memory associated with parameter manager p is freed;
          returns 1 on success, 0 otherwise
*/
int PM_destroy(ParameterManager *p)
{
    Parameter * nextParameter;
    ListElement * node;
    ListElement * temp;
    int index;
    
    /* check that p is valid */
    if (p == NULL)
		return 0;
    
    /* iterate through all parameters */
    for (index = 0; index < p->sizeOfTable; index++)
    {
		/* iterate through hash collision list */
        while (p->parametersTable[index] != NULL)
        {
            nextParameter = p->parametersTable[index]->nextChain;
            /* if parameter is of type list, list must be freed */
            if (p->parametersTable[index]->ptype == LIST_TYPE)
            {
                node = p->parametersTable[index]->value.list_val->elements[0];
                while (node != NULL)
                {
                    temp = node;
                    node = node->next;
                    free(temp->c);
                    free(temp);
                }
                free(p->parametersTable[index]->value.list_val->elements);
                free(p->parametersTable[index]->value.list_val);
            }
            /* if parameter is of type string, string must be freed */
            else if (p->parametersTable[index]->ptype == STRING_TYPE)
            {
                free(p->parametersTable[index]->value.str_val);
            }
            free(p->parametersTable[index]->name);
            free(p->parametersTable[index]);
            
            p->parametersTable[index] = nextParameter;
        }
    }
    
    free(p->parametersTable);
    
	free(p);
    p = NULL;
	if (!p)
    {
		return 1;
    }
	else
    {
		return 0;
    }
}

/*
int PM_parseFrom(ParameterManager *p, FILE *fp, char comment);
	Extract values for parameters from an input stream (see note 2 below)
	PRE: fp is a valid input stream ready for reading that contains the desired parameters
	POST: All required parameters, and those optional parameters present, are assigned values that are consumed from fp, respecting comment as a "start of comment to end of line"
	character if not nul ('\0'); returns non-zero on success, 0 otherwise (parse error,memory allocation failure)
*/
int PM_parseFrom(ParameterManager *p, FILE *fp, char comment)
{
    Parameter * current;
    state_t state = WHITE_STATE;   /* default state is to check for white space */

    char STRING_BUFFER[1025];
    int clearBuffer = 0;
    int ch = 0;
    int spaceCount = 0;
	int index = 0;
    int key;
    char *newString;
    
    /* check that file can be read from */
    if (fp == NULL)
        return 0;
        
    /* check that p is valid */
    if (p == NULL)
		return 0;
    
	while ((ch) != EOF)
	{
        /* printf("Current char is: %c and %d, Current state is: %d, Index is %d, spaceCount: %d\n", ch, ch, state, index, spaceCount); */
    	while((ch = getc(fp)) == comment) 
        {
            /* consume entire line */
			while((ch = getc(fp)) != '\n')
                ;
		}
        switch(state)
        {
            /* WHITE_SPACE
             * Consume all white space */
            case WHITE_STATE:
                /* assume once all the whitespace is gone,
                 * we will enter the EQUAL_STATE */
                state = EQUAL_STATE;
                while(isspace(ch))
                    ch = getc(fp);
                    
                ungetc(ch, fp);
                break;
                
            /* EQUAL_STATE
             * we are looking for the name of a parameter */
            case EQUAL_STATE:
                /* State will be complete once an '=' is found */
                if(ch == '=')
                {
                    
                    /* set flags and end of string character */
                    STRING_BUFFER[index] = '\0';
                    clearBuffer = 1;
                    spaceCount = 0;
                    
                    /* retrieve the Parameter associated with parameter parsed */
                    key = HT_createKey(STRING_BUFFER, p->sizeOfTable);
                    
                    if ((current = HT_findParameter(p->parametersTable[key], STRING_BUFFER)) == NULL)
                    {
						/* ignore unrecognized parameter */
                        while(((ch = getc(fp)) != ';') && (ch != EOF))
                            ;
                        break;
                    }

                    if(current->empty == false)
                    {
                        fputs("PARSE ERROR\nGIVEN PARAMETER ALREADY HAS VALUE ASSIGNED\n", stderr);
                        PM_returnToState(p);
                        return 0;
                    }
                    current->empty = false;
					/* test what type parameter is and switch to given type state */
                    if (current->ptype == STRING_TYPE)
                    {
                        /* If parameter is of STRING_TYPE, we switch to
                         * looking for the first '"' and ignore all white space */
                        while ((ch = getc(fp)) != '\"')
                        {
                            if(!isspace(ch))
                            {
                                fputs("PARSE ERROR\nSTRING VALUE NOT SURRONDED BY \" OR MISSING\n", stderr);
                                PM_returnToState(p);
                                return 0;
                            }
                        }
                        state = STRING_STATE;
                    }
                    if (current->ptype == INT_TYPE)
						state = INT_STATE;
                    if (current->ptype == REAL_TYPE)
						state = REAL_STATE;
					if (current->ptype == BOOLEAN_TYPE)
						state = BOOLEAN_STATE;
					if (current->ptype == LIST_TYPE)
					{
                        /* If parameter is of LIST_TYPE, we switch to
                         * looking for the first '{' and ignore all white space */
						while ((ch = getc(fp)) != '{')
						{
							if (ch == comment)
							{
								while((ch = getc(fp)) != '\n')
									;
							}
							if(!isspace(ch))
                            {
                                fputs("PARSE ERROR\nLIST VALUE NOT SURRONDED BY { OR MISSING\n", stderr);
                                PM_returnToState(p);
                                return 0;
                            }
                        }
						state = LIST_STATE;
					}
                    
                }
                
                /* parameter names are made up of alpha numeric characters,
                 * if one is found add it */
                else if (!(isspace(ch)) && (ch != comment) && (ch != '='))
                {
                    STRING_BUFFER[index] = ch;
                    index++;
                    
                    /* test if any spaces have occured within the parameter
                     * name (PARSE ERROR) */
                    if(spaceCount == 1)
                    {
                        fputs("PARSE ERROR\nINVALID FORMAT\n(POSSIBLE SPACE IN PARAMETER OR DOUBLE SEMI COLON)\n", stderr);
                        PM_returnToState(p);
                        return 0;               /* parse error */
                    }
                }
                else
                {
                    /* white space is allowed, however we do not allow
                     * na me as valid input. Therefore we will track the
                     * number of spaces found, as soon as we find one a flag
                     * is set. */
                    if (isspace(ch) && (index != 0))
                        spaceCount = 1;
                }
                break;
            
            /* STRING_STATE
             * Consume all characters until closing double quote is found */
            case STRING_STATE:
                if(ch != '\"')
                {
                    STRING_BUFFER[index] = ch;
                    index++;
                }
                else
                {
                    STRING_BUFFER[index] = '\0';
                    /* test for an empty string */
                    if(strlen(STRING_BUFFER) == 0)
                    {
                        if(!(newString = malloc(sizeof(char*)*1)))
                        {
							PM_returnToState(p);
                            return 0;
						}
                        newString[0] = '\0';
                    }
                    else
                    {
                        if(!(newString = malloc(sizeof(char*)*strlen(STRING_BUFFER))))
                        {
							PM_returnToState(p);
                            return 0;
                        }
                        strcpy(newString, STRING_BUFFER);
                    }
                    current->value.str_val = newString;
                    current->empty = false;
                    clearBuffer = 1;        /*set flag to reset string buffer */
                    
                    while((ch = getc(fp)) != ';')
                    {
                        if(ch == comment)
                        {
                            /* consume entire line */
                            while((ch = getc(fp)) != '\n')
                                ;
                        }
                        /* only white space (and comments) are allowed
                         * to follow the ending double quote */
                        if(!isspace(ch))
                        {
                            fputs("PARSE ERROR\nINVALID CHARACTERS FOUND AFTER ENDING \"\n", stderr);
                            PM_returnToState(p);
                            return 0;
                        }
                    }
                    state = WHITE_STATE;
                }
                break;
            
            /* INT_STATE
             * Only values of type INT are expected */
			case INT_STATE:
				if(!isspace(ch))
				{
					/* check that ch is a valid int */
					while (((ch >= '0') && (ch <= '9')) || (ch == '-'))
					{
						STRING_BUFFER[index] = ch;
						++index;
						ch = getc(fp);
					}
					current->value.int_val = atoi(STRING_BUFFER);
                    current->empty = false;
					clearBuffer = 1;
					state = WHITE_STATE;
					
                    /* consume all following characters until
                     * the closing semicolon is found */
					while(ch != ';')
                    {
                        if(ch == comment)
                        {
                            /* consume entire line */
                            while((ch = getc(fp)) != '\n')
                                ;
                        }
                        /* anything other than white space here is
                         * a parse error */
                        if(!isspace(ch))
                        {
                            fputs("PARSE ERROR\nINVALID CHARACTERS FOUND AFTER VALUE \"\n", stderr);
                            PM_returnToState(p);
                            return 0;
                        }
                        ch = getc(fp);
                    }
				}
				break;
            
            /* REAL_STATE
             * Only values of type FLOAT are expected */
            case REAL_STATE:
				if(!isspace(ch))
				{
					/* check that ch is a valid int */
					while (((ch >= '0') && (ch <= '9')) || (ch == '.') || (ch == '-'))
					{
						STRING_BUFFER[index] = ch;
						++index;
						ch = getc(fp);
					}
					current->value.real_val = atof(STRING_BUFFER);
                    current->empty = false;
					clearBuffer = 1;
					state = WHITE_STATE;
					
                    /* consume all following characters until
                     * the closing semicolon is found */
					while(ch != ';')
                    {
                        if(ch == comment)
                        {
                            /* consume entire line */
                            while((ch = getc(fp)) != '\n')
                                ;
                        }
                        /* anything other than white space here is
                         * a parse error */
                        if(!isspace(ch))
                        {
                            fputs("PARSE ERROR\nINVALID CHARACTERS FOUND AFTER VALUE\n", stderr);
                            PM_returnToState(p);
                            return 0;               /* parse error */
                        }
                        ch = getc(fp);
                    }
				}
				break;
				
            /* BOOLEAN_STATE
             * only strings of "true" or "false" are expected
             * 0s and 1s will be considered parse errors */
			case BOOLEAN_STATE:
				if(!isspace(ch))
				{
					while ((ch >= 'A') && (ch <= 'z'))
					{
						STRING_BUFFER[index] = ch;
                        ++index;
						ch = getc(fp);
					}
					if (strcasecmp("true", STRING_BUFFER) == 0)
                    {
						current->value.bool_val = true;
                    }
					else if (strcasecmp("false", STRING_BUFFER) == 0)
                    {
						current->value.bool_val = false;
                    }
					else
					{
                        fputs("PARSE ERROR\nINVALID BOOLEAN TYPE\n", stderr);
                        PM_returnToState(p);
						return 0;           /* parse error */
					}
                    /* set flags and change state */
                    current->empty = false;
					clearBuffer = 1;
					spaceCount = 0;
					state = WHITE_STATE;
					
					while(ch != ';')
                    {
                        if(ch == comment)
                        {
                            /* consume entire line */
                            while((ch = getc(fp)) != '\n')
                                ;
                        }
                        if(!isspace(ch))
                        {
                            fputs("PARSE ERROR\nINVALID CHARACTERS FOUND AFTER VALUE\n", stderr);
                            PM_returnToState(p);
                            return 0;       /* parse error */
                        }   
                        ch = getc(fp);
                    }
				}
				break;
				
			/* LIST_STATE
             * Only values of type STRING inside {} are expected */
			case LIST_STATE:
				do
				{
                    /* find first double quotes */
					while (ch != '\"')
                    {
						/* empty list */
						if(ch == '}')
						{
							break;
						}
						if(!isspace(ch) && (ch != '\"') && (ch != comment) && (ch != ','))
                        {
                            fputs("PARSE ERROR\nINVALID CHARACTERS FOUND IN LIST\n", stderr);
                            PM_returnToState(p);
                            return 0;       /* parse error */
                        }   
						if(ch == comment)
                        {
							/* consume entire line */
                            while((ch = getc(fp)) != '\n')
                                ;
                            ch = getc(fp);
                            continue;
						}
						
                        ch = getc(fp);
                        
                        if (ch == '}')
                        {
                            ungetc(ch, fp);     /* put the char back for parsing */
                            break;
                        }
                        /* check for empty list */
                        if (ch == ';')
                        {
							ungetc(ch, fp);
							break;
						}
                        if((!isspace(ch) && (ch != '\"')) && (ch != comment))
                        {
                            fputs("PARSE ERROR\nINVALID CHARACTERS FOUND AFTER ENDING \" IN LIST\n", stderr);
                            PM_returnToState(p);
                            return 0;       /* parse error */
                        }   
                        if(ch == comment)
                        {
                            /* consume entire line */
                            while((ch = getc(fp)) != '\n')
                                ;
                        }
                    }
                    /* loop until string is complete */
					while (((ch = getc(fp)) != '\"') && (ch != '}') && (ch != ';'))
					{
						STRING_BUFFER[index] = ch;
						++index;
					}
					
					if (ch == ';')
					{
						ungetc(ch, fp);
					}
					
					STRING_BUFFER[index] = '\0';
                    if(STRING_BUFFER[0] != '\0')
                    {
                        addListValue(current->value.list_val->elements, STRING_BUFFER);
                        current->empty = false;
                    }
                    current->value.list_val->current = current->value.list_val->elements[0];
                    
					for(; index > 0; index--)
						STRING_BUFFER[index] = '\0';

				} while ((ch = getc(fp)) == ',');
                
                while (ch != ';')
                {
                    if(ch == comment)
                    {
                        /* consume entire line */
                        while((ch = getc(fp)) != '\n')
                            ;
                    }
                    ch = getc(fp);
                }
                state = WHITE_STATE;
				break;
        }
        /* clear the temporary string */
        if (clearBuffer == 1)
        {
            for(; index > 0; index--)
                STRING_BUFFER[index] = '\0';
            clearBuffer = 0;
        }
    }
    
    if (PM_hasRequiredParameters(p) == false)
    {
		PM_returnToState(p);
        return 0;
	}
    return 1;
}

/*
int PM_manage(ParameterManager *p, char *pname, param_t ptype, int required);
	Register parameter for management
	PRE: pname does not duplicate the name of a parameter already managed
	POST: Parameter named pname of type ptype is registered with p as a
	parameter; if required is zero the parameter will be considered optional,
	otherwise it will be considered required; returns 1 on success, 0 otherwise
	(duplicate name, memory allocation failure)
*/
int PM_manage(ParameterManager *p, char *pname, param_t ptype, int required)
{
	Parameter * newParameter;
    int key;
    
    /* check that p is valid */
    if (p == NULL)
		return 0;
    
    /* Find hash key of provided parameter name */
    key = HT_createKey(pname, p->sizeOfTable);
    
    /* Create parameter that is to be added */
    if(!(newParameter = malloc(sizeof(Parameter))))
        return 0;
    if(!(newParameter->name = malloc(sizeof(char)*((int)strlen(pname) + 1))))
        return 0;
    strcpy(newParameter->name, pname);
    newParameter->required = required;
    newParameter->empty = true;
    newParameter->ptype = ptype;
    newParameter->nextChain = NULL;

    if (ptype == LIST_TYPE)
    {   
        /* Create our parameter list if type is a list */
        if(!(newParameter->value.list_val = malloc(sizeof(ParameterList))))
            return 0;
		if(!(newParameter->value.list_val->elements = (ListElement**)malloc(sizeof(ListElement*)*1)))
            return 0;
        newParameter->value.list_val->elements[0] = NULL;
    }
    if (ptype == STRING_TYPE)
    {
		newParameter->value.str_val = NULL;
	}
    
    p->parametersTable[key] = HT_addParameter(p->parametersTable[key], newParameter, pname);
    
    if(p->parametersTable[key] == NULL)
        return 0;
    
    return 1;
}

/*
int PM_hasValue(ParameterManager *p, char *pname);
	Test if a parameter has been assigned a value
	PRE: pname is currently managed by p
	POST: Returns 1 if pname has been assigned a value, 0 otherwise (no value, unknown parameter)
*/
int PM_hasValue(ParameterManager *p, char *pname)
{
    Parameter * test;
    int key;
    key = HT_createKey(pname, p->sizeOfTable);
    key = key % p->sizeOfTable;
    test = HT_findParameter(p->parametersTable[key], pname);
    
    if(test->empty == true)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

/*
union param_value PM_getValue(ParameterManager *p, char *pname);
	Obtain the value assigned to pname
	PRE: pname is currently managed by p and has been assigned a value
	POST: Returns the value (see note 4 below) assigned to pname; result is undefined if pname has not been assigned a value or is unknown
*/
union param_value PM_getValue(ParameterManager *p, char *pname)
{
    Parameter * current;
	int key;
    key = HT_createKey(pname, p->sizeOfTable);
    key = key % p->sizeOfTable;
    current = HT_findParameter(p->parametersTable[key], pname);
    return current->value;
}

/*
Boolean PM_hasRequiredParameters(ParameterManager * manager);
	Check that all required parameters are set
	PRE: manager is a valid parameter manager
	POST: Returns true if all values are present, false otherwise
*/
Boolean PM_hasRequiredParameters(ParameterManager * manager)
{
    Parameter * node;
    int index;
    for (index = 0; index < manager->sizeOfTable; index++)
    {
        node = manager->parametersTable[index];
        
        while((node != NULL) && (node->required == 1))
        {
            if(node->empty == true)
            {
                fputs("PARSE ERROR\nREQUIRED PARAMETER MISSING VALUE\n", stderr);
                return false;
            }
            node = node->nextChain;
        }
    }
    return true;
    
}

/*
Boolean PM_hasRequiredParameters(ParameterManager * manager);
	Check that all required parameters are set
	PRE: manager is a valid parameter manager
	POST: Returns true if all values are present, false otherwise
*/
void PM_returnToState(ParameterManager * p)
{
	Parameter * nextParameter;
    ListElement * node;
    ListElement * temp;
    int index;
    
    /* iterate through all parameters */
    for (index = 0; index < p->sizeOfTable; index++)
    {
		/* iterate through hash collision list */
        while (p->parametersTable[index] != NULL)
        {
            nextParameter = p->parametersTable[index]->nextChain;
            /* if parameter is of type list, list must be freed */
            if (p->parametersTable[index]->ptype == LIST_TYPE)
            {
                node = p->parametersTable[index]->value.list_val->elements[0];
                while (node != NULL)
                {
                    temp = node;
                    node = node->next;
                    free(temp->c);
                    free(temp);
                }
                free(p->parametersTable[index]->value.list_val->elements);
                free(p->parametersTable[index]->value.list_val);
            }
            /* if parameter is of type string, string must be freed */
            else if (p->parametersTable[index]->ptype == STRING_TYPE)
            {
                free(p->parametersTable[index]->value.str_val);
            }
            p->parametersTable[index] = nextParameter;
        }
    }
}

