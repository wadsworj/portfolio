#include "cis2750_a2_GUI_Interface.h"
#include "ParameterManager.h"
#include <jni.h>



JNIEXPORT jstring JNICALL Java_cis2750_1a2_GUI_1Interface_PM_1Create
  (JNIEnv * env, jclass obj, jstring filePath, jstring workingDir, jstring fileName)
{
    FILE * fp;
	ParameterManager * initialParameters;
    ParameterManager * elementParameters;
    
    ParameterList * fieldsList;
    
    char * fieldParameter;
    char * projectTitle;
    
    const jbyte *fName;
    const jbyte *fPath;
    const jbyte *workingDirectory;
    
    fName = (*env)->GetStringUTFChars(env, fileName, NULL);
    fPath = (*env)->GetStringUTFChars(env, filePath, NULL);
    workingDirectory = (*env)->GetStringUTFChars(env, workingDir, NULL);

	/* p will contain title, fields, buttons */
	initialParameters = PM_create(3);
	PM_manage(initialParameters, "title", STRING_TYPE, 1);
	PM_manage(initialParameters, "fields", LIST_TYPE, 1);
	PM_manage(initialParameters, "buttons", LIST_TYPE, 1);
    
	printf("File name: %s\n", (char *)fPath);
    
    fp = fopen((char *)fPath, "r");
    
    PM_parseFrom(initialParameters, fp, '#');
    
    if ((elementParameters = PM_create(10)) == NULL)
        return 0;
    
    fieldsList = PM_getValue(initialParameters, "fields").list_val;
    
    while ((fieldParameter = PL_next(fieldsList)))
    {
        printf("%s", fieldParameter);
        
        PM_manage(elementParameters, fieldParameter, STRING_TYPE, 0);
    }
    
    /* close file and reopen to read again */
    if((fclose(fp) == EOF))
        return 0;
    fp = fopen((char *)fPath, "r");
    
    PM_destroy(initialParameters);
    
    PM_manage(elementParameters, "title", STRING_TYPE, 1);
	PM_manage(elementParameters, "fields", LIST_TYPE, 1);
	PM_manage(elementParameters, "buttons", LIST_TYPE, 1);
    
    PM_parseFrom(elementParameters, fp, '#');
    
    
    generateInterface(elementParameters, (char*)fName, (char*)workingDirectory);
    generateCompiled(elementParameters, (char*)fName, (char*)workingDirectory);
    
    projectTitle = PM_getValue(elementParameters, "title").str_val;
    
    return ((*env)->NewStringUTF(env, projectTitle));
}
