/*
 * 2750_A1 - Winter 2015
 * Name: James Wadsworth,
 * User ID: 0813414
*/

#include "ParameterManager.h"

/*
A parameter list is a sequence of individual values (string type) that taken together constitute the value assigned to a single parameter
Operations (as C function prototypes):

char * PL_next(ParameterList *l);
	Obtain the next item in a parameter list
	PRE: n/a
	POST: Returns the next item in parameter list l, NULL if there are no items remaining in the list (see note below)
*/
char * PL_next(ParameterList *l)
{
    char * temp;
    
    if(l->current == NULL)
        return NULL;
    
    temp = l->current->c;
    l->current = l->current->next;
    
    return temp;
}

int PL_reset(ParameterList * list)
{
    list->current = list->elements[0];
    return 1;
}
