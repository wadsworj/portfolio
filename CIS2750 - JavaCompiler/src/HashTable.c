/*
 * 2750_A1 - Winter 2015
 * Name: James Wadsworth,
 * User ID: 0813414
*/

#include "ParameterManager.h"

/*
A function to determine the next closest prime number, to be used in
* our hash function. An array of prime numbers is declatered and the
* int passed in is checked against each number to determine the closest
* number.

int nextPrime(int number);
	Obtain the next prime number from a list
	PRE: n/a
	POST: Returns the next largest prime number, if number provided is larger than
          this number, the largest prime in the list is returned.
*/
int HT_nextPrime(int number)
{
    int index = 0;
    int diff1;
    int diff2 = 0;
    int primes[18] = {7, 13, 43, 59, 89, 103, 157, 193, 211,
                        251, 269, 311, 373, 389, 433, 449, 491, 509};
    diff2 = abs(number - primes[index]);

    for (index = 0; index < 18; index++)
    {
        diff1 = abs(number - primes[index]);
        
        if (diff2 < diff1)
            return primes[index - 1];
        diff2 = diff1;
    }
    /* else return largest prime from list */
    return primes[17];
}

int HT_createKey(char * string, int hashSize)
{
    int index = 0;
    int key = 0;
    while (string[index] != '\0')
    {
        key += string[index];
        ++index;
    }
    key = key % hashSize;
    return key;
}

Parameter * HT_findParameter(Parameter * root, char* name)
{
    Parameter * node;
    
    if(root == NULL)
        return NULL;        /* parse error */
    
    node = root;
    
    /* test if name is found */
    while (!checkIfEqual(node, name))
    { 
        
        if(node->nextChain != NULL)
        {
            node = node->nextChain;
        }
        else
        {
            return NULL;        /* parse error */
        }
    }
    return node;
}

Parameter * HT_addParameter(Parameter * root, Parameter * newParameter, char* name)
{
    Parameter * node;
    
    if (root == NULL)
    {
        root = newParameter;
        return root;
    }
    node = root;
    
    if (checkIfEqual(node, name))
            return NULL;
    
    while (node->nextChain != NULL)
    {
        /* test if name is found */
        if (checkIfEqual(node, name))
            return 0;
        node = node->nextChain;
    }
    
    node->nextChain = newParameter;
    
    return root;
}


Boolean checkIfEqual(Parameter * node, char* name)
{
    if (strcmp(node->name, name) != 0)
        return false;
    return true;
}
