#include "ParameterManager.h"

int generateInterface(ParameterManager * manager, char * fileName, char * workingDirectory)
{
    FILE * interface;
    
    ParameterList * fieldsList;
    struct stat st = {0};
    char * fieldName;
    char buffer[65];
    
    int numberOfFields = 0;
    
    /* create file for interface */
    
    strcpy(buffer, workingDirectory);
    strcat(buffer, "/");
    strcat(buffer, fileName);
    strcat(buffer, "/");

    /* check if generated folder needs to be created */
    if (stat(buffer, &st) == -1)
    {
        mkdir(buffer, 0700);
    }
    
    
    strcpy(buffer, workingDirectory);
    strcat(buffer, "/");
    strcat(buffer, fileName);
    strcat(buffer, "/");
    strcat(buffer, fileName);
    strcat(buffer, "FieldEdit.java");
    
    printf("title: %s\n", buffer);
    
    if((interface = fopen(buffer, "w")) == NULL)
    {
        printf("Error\n");
        return 0;
    }
    
    /* import statements */
    fprintf(interface, "/* %s */\n\n", buffer);
    fprintf(interface, "package %s;\n\n", fileName);
    fprintf(interface, "import java.awt.*;\n");
    fprintf(interface, "import javax.swing.*;\n");
    fprintf(interface, "import javax.swing.border.BevelBorder;\n\n");
    
    /* Interface */
    fprintf(interface, "/* Interface: %sFieldEdit */\n", fileName);
    fprintf(interface, "public interface %sFieldEdit\n", fileName);
    fprintf(interface, "{\n");
    
    fieldsList = PM_getValue(manager, "fields").list_val;
    
    while ((fieldName = PL_next(fieldsList)))
    {
        fprintf(interface, "\tpublic String getDC%s();\n", fieldName);
        fprintf(interface, "\tpublic void setDC%s(String string);\n\n", fieldName);
        numberOfFields++;
    }
    fprintf(interface, "}\n\n\n");
    
    PL_reset(fieldsList);
    
    fclose(interface);
    
    return 1;
}

/* int generateCompiled(ParameterManager * manager)
 */
int generateCompiled(ParameterManager * manager, char * fileName, char * workingDirectory)
{
    FILE * interface;
    
    ParameterList * fieldsList;
    ParameterList * buttonsList;
    struct stat st = {0};
    char * titleName;
    char * fieldName;
    char * buttonName;
    char buffer[125];
    
    /* create file for interface */
    titleName = PM_getValue(manager, "title").str_val;
    strcpy(buffer, workingDirectory);
    strcat(buffer, "/");
    strcat(buffer, fileName);
    strcat(buffer, "/");
    
    
    /* check if generated folder needs to be created */
    if (stat(buffer, &st) == -1)
    {
        mkdir(buffer, 0700);
    }
    
    
    strcpy(buffer, workingDirectory);
    strcat(buffer, "/");
    strcat(buffer, fileName);
    strcat(buffer, "/");
    strcat(buffer, fileName);
    strcat(buffer, ".java");
    
    printf("title: %s\n", buffer);
    
    if((interface = fopen(buffer, "w")) == NULL)
    {
        printf("Error\n");
        return 0;
    }
    /* import statements */
    fprintf(interface, "/* %s */\n\n", buffer);
    fprintf(interface, "package %s;\n\n", fileName);
    fprintf(interface, "import java.awt.*;\n");
    fprintf(interface, "import javax.swing.*;\n");
    fprintf(interface, "import javax.swing.border.BevelBorder;\n\n");
    
    /* Class */
    fprintf(interface, "public class %s extends JFrame implements %sFieldEdit\n", fileName, fileName);
    fprintf(interface, "{\n");
    /* Main file */
    fprintf(interface, "\tpublic static void main(String[] args)\n");
    fprintf(interface, "\t{\n\t\tnew %s();\n\t}\n", fileName);
    /* Declare JTextFields */
    
    fieldsList = PM_getValue(manager, "fields").list_val;
    
    while ((fieldName = PL_next(fieldsList)))
    {
        fprintf(interface, "\tprivate JTextField %s;\n", fieldName);
    }
    fprintf(interface, "\n\tpublic %s()\n", fileName);
    fprintf(interface, "\t{\n");
    fprintf(interface, "\n\t\tsetTitle(\"%s\");", titleName);
    fprintf(interface, "\n\t\tsetSize(451, 520);");
    fprintf(interface, "\n\t\tsetDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);\n");
    
    fprintf(interface, "\n\t\tJPanel panel = new JPanel();\n");
    fprintf(interface, "\t\tpanel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));\n");
    
    fprintf(interface, "\n\t\tJPanel fieldsPanel = new JPanel();\n");
    fprintf(interface, "\t\tGridLayout grid = new GridLayout(0, 2);\n");
    fprintf(interface, "\t\tgrid.setHgap(10);\n");
    
    fprintf(interface, "\n\t\tfieldsPanel.setLayout(grid);\n\n");
    PL_reset(fieldsList);
    while ((fieldName = PL_next(fieldsList)))
    {
        
        fprintf(interface, "\t\t%s = new JTextField();\n", fieldName);
        fprintf(interface, "\t\tfieldsPanel.add(new JLabel(\"%s\"));\n", fieldName);
        fprintf(interface, "\t\tfieldsPanel.add(%s);\n\n", fieldName);
        
    }
    
    fprintf(interface, "\t\tpanel.add(fieldsPanel, BorderLayout.NORTH);\n");
    
    
    fprintf(interface, "\n\t\t/* Buttons Panel */\n");
    fprintf(interface, "\t\tJPanel buttonsPanel = new JPanel();\n");
    fprintf(interface, "\t\tbuttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));\n");
    
    buttonsList = PM_getValue(manager, "buttons").list_val;
    
    while ((buttonName = PL_next(buttonsList)))
    {
        fprintf(interface, "\t\tJButton %sButton = new JButton(\"%s\");\n", buttonName, buttonName);
        fprintf(interface, "\t\tbuttonsPanel.add(%sButton);\n\n", buttonName);
    }
    
    fprintf(interface, "\t\tpanel.add(buttonsPanel, BorderLayout.CENTER);\n");
    
    fprintf(interface, "\n\t\tJPanel statusPanel = new JPanel();\n");
    fprintf(interface, "\t\tstatusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));\n");
    fprintf(interface, "\t\tJLabel statusLabel = new JLabel(\"STATUS\");\n");
    fprintf(interface, "\t\tJTextField statusText = new JTextField();\n");
    fprintf(interface, "\t\tstatusPanel.add(statusLabel);\n");
    fprintf(interface, "\t\tpanel.add(statusPanel, BorderLayout.CENTER);\n");
    fprintf(interface, "\t\tpanel.add(statusText, BorderLayout.CENTER);\n");
    fprintf(interface, "\t\tadd(panel);\n");
    fprintf(interface, "\t\t\n");
    fprintf(interface, "\t\t\n");
    
    fprintf(interface, "\n\t\tsetVisible(true);\n");
    
    fprintf(interface, "\t}\n\n");
    
    /* Getters and Setters */
    
    PL_reset(fieldsList);
    
    fprintf(interface, "\t/* Getters and setters */\n");
    
    while ((fieldName = PL_next(fieldsList)))
    {
        fprintf(interface, "\tpublic String getDC%s()\n", fieldName);
        fprintf(interface, "\t{\n");
        fprintf(interface, "\t\treturn %s.getText();\n", fieldName);
        fprintf(interface, "\t}\n");
        
        fprintf(interface, "\tpublic void setDC%s(String string)\n", fieldName);
        fprintf(interface, "\t{\n");
        fprintf(interface, "\t\t%s.setText(string);\n", fieldName);
        fprintf(interface, "\t}\n");
    }
    fprintf(interface, "}\n\n\n");
    
    fclose(interface);
    
    return 1;
}
