/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cis2750_a2;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author James Wadsworth
 */
public class EditorPanel extends JPanel implements DocumentListener {
    
    private static JTextArea editor;
    private static TitledBorder border;
    private static String currentPath;
    
    EditorPanel()
    {
        currentPath = "./";
        editor = new JTextArea(30,85);
        border = new TitledBorder("untitled");
        editor.setBorder(border);
        editor.setFont(new Font("monospaced", Font.PLAIN, 12));
        
        editor.getDocument().addDocumentListener(this);
        
        JScrollPane scrollPane = new JScrollPane(editor);
        
        add(scrollPane);
        
        setVisible(true);
    }
    
    public static void setBorderText(String string)
    {
        border = new TitledBorder(string);
        editor.setBorder(border);
    }
    
    public void setTextArea(String text)
    {
        editor.setText(text);
    }
    
    public String getBorderText()
    {
        System.out.println("We are getting " + border.getTitle());
        return border.getTitle().replaceFirst("[.][^.]+$", "");
        /* above line taken from Stackoverflow, "How to get file name without the extension?"*/
    }

    public String getText() {
        return editor.getText();
    }
    
    public void setCurrentPath(String string)
    {
        currentPath = string;
    }
    
    public String getCurrentPath()
    {
        return currentPath;
    }
    
    /* Document Listener Functions */
    public void removeUpdate(DocumentEvent e)
    {
	System.out.println("Remove Update");
        //GUI_Interface.fileEdited();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
	System.out.println("Insert Update");
        GUI_Interface.fileEdited();
    }
    @Override
    public void changedUpdate(DocumentEvent e)
    {
    	System.out.println("Changed Update");
        GUI_Interface.fileEdited();
    }
}
