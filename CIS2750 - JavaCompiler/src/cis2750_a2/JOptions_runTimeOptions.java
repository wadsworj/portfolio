package cis2750_a2;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;



public class JOptions_runTimeOptions{

	private static JTextField textField;
	
	public JOptions_runTimeOptions()
	{
		JPanel textPanel = new JPanel();
		
		textField = new JTextField(GUI_Interface.getJavaOptions(), 20);
	
		textPanel.add(textField);

		if ((JOptionPane.showConfirmDialog(GUI_Interface.getMainFrame(), textPanel,
							/* option pane title */ "Confirm Save",
							/* option pane buttons */ JOptionPane.OK_CANCEL_OPTION))
							/* if yes */ == JOptionPane.OK_OPTION) {
                GUI_Interface.setJavaOptions(textField.getText());
		} else {
		
		}
	}
}
