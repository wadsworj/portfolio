package cis2750_a2;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;



public class JOptions_workingDirectory{

	private static JTextField textField;
	
	public JOptions_workingDirectory()
	{
		JPanel textPanel = new JPanel();
		
		textField = new JTextField(GUI_Interface.getWorkingDir(), 20);
	
		JButton browseButton = new JButton("Browse");
	
		textPanel.add(textField);
	
		textPanel.add(browseButton);
	
		browseButton.addActionListener(new ActionListener()
		{
		    public void actionPerformed(ActionEvent e) { 
                
			JFileChooser chooser = new JFileChooser(GUI_Interface.getWorkingDir());
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			chooser.setMultiSelectionEnabled(false);
                
			//FileNameExtensionFilter filter = new FileNameExtensionFilter(
			//	 ".config file", "config");
			//chooser.setFileFilter(filter);
			int returnVal = chooser.showOpenDialog(GUI_Interface.getMainFrame());
			if(returnVal == JFileChooser.APPROVE_OPTION)
			{
			     //System.out.println("You chose to open this file : " +
			     //chooser.getSelectedFile().getName());
		     
			     textField.setText(chooser.getSelectedFile().getAbsolutePath());
			}
		    }
		});

		if ((JOptionPane.showConfirmDialog(GUI_Interface.getMainFrame(), textPanel,
							/* option pane title */ "Confirm Save",
							/* option pane buttons */ JOptionPane.OK_CANCEL_OPTION))
							/* if yes */ == JOptionPane.OK_OPTION) {
                GUI_Interface.setWorkingDir(textField.getText());
		} else {
		
		}
	}
}
