

package cis2750_a2;


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.*;



public class GUI_About extends JFrame {
    GUI_About() {
        super("About");
        
        this.setVisible(true);
        setSize(400, 300);
        setResizable(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        JPanel aboutPanel = new JPanel();
        aboutPanel.setLayout(new BoxLayout(aboutPanel, BoxLayout.Y_AXIS));
	
	
        JTextArea aboutText = new JTextArea();
        
        aboutText.setText("Name: James Wadsworth\n"
                            + "Student ID: 0813414");
        
        JButton dismissButton = new JButton("Dismiss");
        
        dismissButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                closeAbout();
            }
        });
        
        aboutPanel.add(aboutText, BorderLayout.NORTH);
        aboutPanel.add(dismissButton, BorderLayout.SOUTH);
        
        add(aboutPanel);
        
    }
    
    public void closeAbout()
    {
        this.dispose();
    }
}
