package cis2750_a2;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;



public class JOptions_compile{

    private static JTextField textField;
    
    public JOptions_compile()
    {
        JPanel textPanel = new JPanel();
        
        textField = new JTextField(GUI_Interface.getJavaCompiler(), 20);
    
        JButton browseButton = new JButton("Browse");
    
        textPanel.add(textField);
    
        textPanel.add(browseButton);
    
        browseButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                JFileChooser chooser = new JFileChooser(GUI_Interface.getWorkingDir());
                chooser.setMultiSelectionEnabled(false);
                    
                int returnVal = chooser.showOpenDialog(GUI_Interface.getMainFrame());
                if(returnVal == JFileChooser.APPROVE_OPTION)
                {
                     textField.setText(chooser.getSelectedFile().getAbsolutePath());
                }
            }
        });

        if ((JOptionPane.showConfirmDialog(GUI_Interface.getMainFrame(), textPanel,
                            /* option pane title */ "Confirm Save",
                            /* option pane buttons */ JOptionPane.OK_CANCEL_OPTION))
                            /* if yes */ == JOptionPane.OK_OPTION) {
                GUI_Interface.setJavaCompiler(textField.getText());
        } else {
        
        }
    }
}
