
package cis2750_a2;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.*;


public class GUI_Help extends JFrame {
    public GUI_Help() {
        super("Help");
        
        String in;
        String saved = "";
        
        this.setVisible(true);
        setSize(600, 600);
        setResizable(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        JPanel aboutPanel = new JPanel();
        
        JTextArea readMeText = new JTextArea();
        
        setLocation(150, 150);
        
        try
        {
            BufferedReader bufferReader = new BufferedReader(new FileReader("./readme.txt"));
        
        
            while ((in = bufferReader.readLine()) != null)
            {
                saved += in;
                saved += '\n';
            }
            readMeText.setText(saved);
        }
        catch (IOException e)
        {
            
        }
	
        aboutPanel.add(readMeText);
        
	
	JScrollPane scrollPane = new JScrollPane(aboutPanel);
	
        add(scrollPane);
        
        
        setVisible(true);
        
    }
}
