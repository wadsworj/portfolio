package cis2750_a2;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import javax.swing.filechooser.*;



public class MenuBar_Actions
{

	static void quitProgram()
    {
        if (GUI_Interface.getModified() == true)
        {
            GUI_Interface.checkSave("close");
	    	System.exit(0);
        }
        else
        {
            System.exit(0);
        }
    }
    
	static void saveAsFile()
    {
        File file;
        JFileChooser chooser = new JFileChooser(GUI_Interface.getWorkingDir());
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                 ".config file", "config");
        
        chooser.setFileFilter(filter);
        chooser.setSelectedFile(new File(GUI_Interface.getMiddleFrame().getCurrentPath()));
        
        int returnVal = chooser.showSaveDialog(GUI_Interface.getMainFrame());
        if(returnVal == JFileChooser.APPROVE_OPTION)
        {
        }
        file = chooser.getSelectedFile();
        
        if (!file.exists())
        {
            try
            {
                file.createNewFile();
                
                FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
        		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        		bufferedWriter.write(GUI_Interface.getMiddleFrame().getText());
        		bufferedWriter.close();
            }
            catch (IOException e)
            {
                
            }
        }
    }

    public static void compile()
    {
    	String projectTitle;
        projectTitle = GUI_Interface.PM_Create(GUI_Interface.getMiddleFrame().getCurrentPath(), GUI_Interface.getWorkingDir(), GUI_Interface.getMiddleFrame().getBorderText());
    }

    public static void compileAndRun()
    {
    	String projectTitle;
        try
        {
            projectTitle = GUI_Interface.PM_Create(GUI_Interface.getMiddleFrame().getCurrentPath(), GUI_Interface.getWorkingDir(), GUI_Interface.getMiddleFrame().getBorderText());
            projectTitle = GUI_Interface.getMiddleFrame().getBorderText();
            Process compile = Runtime.getRuntime().exec(GUI_Interface.getJavaCompiler() + " " + GUI_Interface.getJavaCompilerOptions()
                            + " " + GUI_Interface.getWorkingDir() + "/" + projectTitle + "/" +  projectTitle + ".java");
                            
            compile.waitFor();
            Process run = Runtime.getRuntime().exec(GUI_Interface.getJVM() + " " + GUI_Interface.getJavaOptions() + " " + projectTitle + "."
                            +  projectTitle);
        }
        catch (Exception e)
        {
            System.out.println("failure");
        }
    }

}
