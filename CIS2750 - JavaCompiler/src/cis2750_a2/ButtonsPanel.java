/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cis2750_a2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.FlowLayout;
import java.awt.Image;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 *
 * @author James Wadsworth
 */
public class ButtonsPanel extends JPanel {
    ButtonsPanel() {
        
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        
        ImageIcon newIcon = new ImageIcon("images/toolbarButtonGraphics/general/New24.gif");
        ImageIcon openIcon = new ImageIcon("images/toolbarButtonGraphics/general/Open24.gif");
        ImageIcon saveIcon = new ImageIcon("images/toolbarButtonGraphics/general/Save24.gif");
        ImageIcon saveAsIcon = new ImageIcon("images/toolbarButtonGraphics/general/SaveAs24.gif");
        ImageIcon buildIcon = new ImageIcon("images/toolbarButtonGraphics/media/Stop24.gif");
        ImageIcon runIcon = new ImageIcon("images/toolbarButtonGraphics/media/Play24.gif");
        ImageIcon compileAndRunIcon = new ImageIcon("images/toolbarButtonGraphics/general/Stop24.gif");        
       
        JButton newButton = new JButton(newIcon);
        JButton openButton = new JButton(openIcon);
        JButton saveButton = new JButton(saveIcon);
        JButton saveAsButton = new JButton(saveAsIcon);
        JButton buildButton = new JButton(buildIcon);
        JButton runButton = new JButton(runIcon);
        JButton compileAndRunButton = new JButton(compileAndRunIcon);
        
        newButton.setHorizontalAlignment(SwingConstants.LEFT);
        openButton.setHorizontalAlignment(SwingConstants.LEFT);
        saveButton.setHorizontalAlignment(SwingConstants.LEFT);
        saveAsButton.setHorizontalAlignment(SwingConstants.LEFT);
        buildButton.setHorizontalAlignment(SwingConstants.LEFT);
        runButton.setHorizontalAlignment(SwingConstants.LEFT);
        compileAndRunButton.setHorizontalAlignment(SwingConstants.LEFT);
        
        add(newButton);
        add(openButton);
        add(saveButton);
        add(saveAsButton);
        add(buildButton);
        add(runButton);
        add(compileAndRunButton);
        
        /* action listeners for all buttons */
        
        newButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {GUI_Interface.newFile();}
        });
        
        openButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {GUI_Interface.openFile();}
        });
        
        saveButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {GUI_Interface.saveFile();}
        });
        
        saveAsButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {MenuBar_Actions.saveAsFile();}
        });
        
        buildButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {MenuBar_Actions.compile();}
        });
        
        runButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {MenuBar_Actions.compileAndRun();}
        });
        
        compileAndRunButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {MenuBar_Actions.compileAndRun();}
        });
        
        setVisible(true);
    }
            
}
