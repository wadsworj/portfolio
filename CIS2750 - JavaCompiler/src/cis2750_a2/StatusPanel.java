/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cis2750_a2;

import javax.swing.*;
import javax.swing.border.BevelBorder;

/**
 *
 * @author James Wadsworth
 */
public class StatusPanel extends JPanel {
    
    private JLabel status;
    
    StatusPanel() {
        
        status = new JLabel("Current Project: untitled");
        setBorder(new BevelBorder(BevelBorder.LOWERED));
        
        add(status);
        
        setVisible(true);
    }
    
    public void setStatus(String text)
    {
        status.setText(text);
    }
    
    public void updateStatus(String text)
    {
        status.setText("Current Project: " + text);
    }
}
