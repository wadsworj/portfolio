/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cis2750_a2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author James Wadsworth
 */
class MenuBar extends JMenuBar implements ActionListener {

    public MenuBar()
    {
        // JMenus for MenuBar
        JMenu fileMenu = new JMenu("File");
        JMenu compileMenu = new JMenu("Compile");
        JMenu configMenu = new JMenu("Config");
        JMenu helpMenu = new JMenu("Help");
        
        // JMenuItems for "File"
        JMenuItem newMenuItem = new JMenuItem("New");
        JMenuItem openMenuItem = new JMenuItem("Open");
        JMenuItem saveMenuItem = new JMenuItem("Save");
        JMenuItem saveasMenuItem = new JMenuItem("Save As");
        JMenuItem quitMenuItem = new JMenuItem("Quit");
        
        newMenuItem.addActionListener(this);
        openMenuItem.addActionListener(this);
        saveMenuItem.addActionListener(this);
        saveasMenuItem.addActionListener(this);
        quitMenuItem.addActionListener(this);
        
        fileMenu.add(newMenuItem);
        fileMenu.add(openMenuItem);
        fileMenu.add(saveMenuItem);
        fileMenu.add(saveasMenuItem);
        fileMenu.add(quitMenuItem);
        
        // JMenuItems for "Compile"
        JMenuItem compileMenuItem = new JMenuItem("Compile");
        JMenuItem compileAndRunMenuItem = new JMenuItem("Compile and run");
        
        compileMenu.add(compileMenuItem);
        compileMenu.add(compileAndRunMenuItem);

        compileMenuItem.addActionListener(this);
        compileAndRunMenuItem.addActionListener(this);
        
        // JMenuItems for "Config"
        JMenuItem javaCompilerMenuItem = new JMenuItem("Java Compiler");
        JMenuItem compileOptionsMenuItem = new JMenuItem("Compile options");
        JMenuItem javaRunTimeMenuItem = new JMenuItem("Java Run-time");
        JMenuItem runTimeOptionsMenuItem = new JMenuItem("Run-time options");
        JMenuItem workingDirectoryMenuItem = new JMenuItem("Working Directory");

	    javaCompilerMenuItem.addActionListener(this);
        compileOptionsMenuItem.addActionListener(this);
        javaRunTimeMenuItem.addActionListener(this);
        runTimeOptionsMenuItem.addActionListener(this);
        workingDirectoryMenuItem.addActionListener(this);

        configMenu.add(javaCompilerMenuItem);
        configMenu.add(compileOptionsMenuItem);
        configMenu.add(javaRunTimeMenuItem);
        configMenu.add(runTimeOptionsMenuItem);
        configMenu.add(workingDirectoryMenuItem);
        
        // JMenuItems for "Help"
        JMenuItem helpMenuItem = new JMenuItem("Help");
        JMenuItem aboutMenuItem = new JMenuItem("About");
        
        aboutMenuItem.addActionListener(this);
        helpMenuItem.addActionListener(this);
        
        helpMenu.add(helpMenuItem);
        helpMenu.add(aboutMenuItem);
        
        add(fileMenu);
        add(compileMenu);
        add(configMenu);
        add(helpMenu);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        String test = e.getActionCommand();
        
        if (test.equals("New"))
        {
            GUI_Interface.newFile();
        }
        else if (test.equals("Open"))
        {   
            GUI_Interface.openFile();
        }
        else if (test.equals("Save"))
        {   
            GUI_Interface.saveFile();
        }
        else if (test.equals("Save As"))
        {   
            MenuBar_Actions.saveAsFile();
        }
        else if (test.equals("Quit"))
        {
            MenuBar_Actions.quitProgram();
        }
        else if (test.equals("Compile"))
        {   
            GUI_Interface.saveFile();
            MenuBar_Actions.compile();
        }
        else if (test.equals("Compile and run"))
        {   
            GUI_Interface.saveFile();
            MenuBar_Actions.compileAndRun();
        }
        /* Java options */
        else if (test.equals("Java Compiler"))
        {   
            new JOptions_compile();
        }
        else if (test.equals("Compile options"))
        {   
            new JOptions_compileOptions();
        }
        else if (test.equals("Java Run-time"))
        {   
            new JOptions_runTime();
        }
        else if (test.equals("Run-time options"))
        {   
            new JOptions_runTimeOptions();
        }
        else if (test.equals("Working Directory"))
        {
		System.out.println("We got here");
            new JOptions_workingDirectory();
        }
        /* Help Options */
        else if (test.equals("Help"))
        {
            new GUI_Help();
        }
        else if (test.equals("About"))
        {
            new GUI_About();
        }
        else
        {
            // Nothing
        }
        
        
    }
    
}
