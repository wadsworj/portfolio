/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cis2750_a2;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import javax.swing.*;
import javax.swing.filechooser.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author James Wadsworth
 */
public class GUI_Interface
{
    
    
    public static final int WIDTH = 670;
    public static final int HEIGHT = 740;
    
    private static JFrame mainFrame;
    private static Boolean modified;
    
    private static EditorPanel middle;
    private static StatusPanel bottom;
    
    private static String projectTitle;
    private static String workingDir;
    private static String jvmVariable;
    private static String javaOptions;
    private static String javaCompiler;
    private static String javaCompilerOptions;
    
    public GUI_Interface()
    {
        modified = false;
        workingDir = ".";

        jvmVariable = "java";

        javaOptions = (" -cp " + workingDir);

        javaCompiler = "javac";
        javaCompilerOptions = ("-cp " + workingDir);

        mainFrame = new JFrame();
        mainFrame.setSize(WIDTH, HEIGHT);
        mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        
        mainFrame.addWindowListener(new WindowListener()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                MenuBar_Actions.quitProgram();
            }
            /* non implemented WindowListeners */
            public void windowOpened(WindowEvent we) {}
            public void windowClosed(WindowEvent we) {}
            public void windowIconified(WindowEvent we) {}
            public void windowDeiconified(WindowEvent we) {}
            public void windowActivated(WindowEvent we) {}
            public void windowDeactivated(WindowEvent we) {}
            
        });
        
        mainFrame.setJMenuBar(new MenuBar());
        
        mainFrame.setLayout(new BorderLayout());

        ButtonsPanel top = new ButtonsPanel();
        middle = new EditorPanel();
        bottom = new StatusPanel();
        
        middle.setPreferredSize(new Dimension(400, 400));
        
        mainFrame.add(top, BorderLayout.NORTH);
        mainFrame.add(middle, BorderLayout.CENTER);
        mainFrame.add(bottom, BorderLayout.SOUTH);
        
        mainFrame.setVisible(true);
    }
    
    static void openFile()
    {
        BufferedReader bufferReader;
        File file;
        String in = null;
        String saved = "";
        
	if (modified == true)
        {
            checkSave("open a new file");
        }
	
        JFileChooser chooser = new JFileChooser(workingDir);
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                 ".config file", "config");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(mainFrame);
        if(returnVal == JFileChooser.APPROVE_OPTION)
        {
             //System.out.println("You chose to open this file : " +
             //chooser.getSelectedFile().getName());
        }
        
        file = chooser.getSelectedFile();
        
        try
        {
            bufferReader = new BufferedReader(new FileReader(file));
            
            while ((in = bufferReader.readLine()) != null)
            {
                saved += in;
                saved += '\n';
            }
            middle.setTextArea(saved);
            middle.setBorderText(chooser.getSelectedFile().getName());
            middle.setCurrentPath(chooser.getSelectedFile().getAbsolutePath());
            bottom.updateStatus(chooser.getSelectedFile().getName());
        }
        catch (IOException e)
        {
        }
        modified = false;
    }
    static void newFile()
    {
	if (modified == true)
        {
            checkSave("create a new file");
        }
	
	middle.setTextArea("");
	middle.setBorderText("untitled");
	bottom.updateStatus("untitled");
    }
    
    static void saveFile()
    {
        if (middle.getBorderText().equals("untitled"))
            MenuBar_Actions.saveAsFile();
        else
        {
            try
            {
                System.out.println("saving.." + middle.getCurrentPath());
                PrintWriter out = new PrintWriter(middle.getCurrentPath());
                out.println(middle.getText());
                out.close();

                bottom.updateStatus(middle.getBorderText());
                modified = false;
            }
            catch (IOException e)
            {

            }
        }
    }
    
    static void fileEdited()
    {
		System.out.println("File modified");
        modified = true;
        bottom.updateStatus(middle.getBorderText() + " [modified]");
    }
    
    
    static void checkSave(String text)
    {
		modified = false;
		if ((JOptionPane.showConfirmDialog(mainFrame, "Do you want to save before you " + text + "?",
						/* option pane title */ "Confirm Save",
						/* option pane buttons */ JOptionPane.YES_NO_OPTION))
						/* if yes */ == JOptionPane.YES_OPTION) {
					GUI_Interface.saveFile();
		}
    }

    native static String PM_Create(String filePath, String workingDir, String fileName);
    static
    {
        System.loadLibrary("JNIpm");
    }
    
    public static JFrame getMainFrame()
    {
        return mainFrame;
    }

    public static EditorPanel getMiddleFrame()
    {
        return middle;
    }

    public static String getProjectTitle()
    {
        return projectTitle;
    }

    public static Boolean getModified()
    {
        return modified;
    }
    
    public static void setWorkingDir(String text)
    {
        workingDir = text;
    }
    
    public static String getWorkingDir()
    {
        return workingDir;
    }
    
    public static void setJVM(String text)
    {
        jvmVariable = text;
    }
    
    public static String getJVM()
    {
        return jvmVariable;
    }
    
    public static void setJavaOptions(String text)
    {
        javaOptions = text;
    }
    
    public static String getJavaOptions()
    {
        return javaOptions;
    }

    public static void setJavaCompiler(String text)
    {
        javaCompiler = text;
    }
    
    public static String getJavaCompiler()
    {
        return javaCompiler;
    }

    public static void setJavaCompilerOptions(String text)
    {
        javaCompilerOptions = text;
    }
    
    public static String getJavaCompilerOptions()
    {
        return javaCompilerOptions;
    }
}
