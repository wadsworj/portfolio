/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;


import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import javax.swing.*;


/**
 *
 * @author James Wadsworth
 */
public class AdjustOdds extends JFrame implements ActionListener {
    
    private static Integer[] odds1;
    private static Integer[] odds2;
    private static Integer[] odds3;
    
    private static JTextField cherry1;
    private static JTextField cherry2;
    private static JTextField cherry3;
    
    private static JTextField bell1;
    private static JTextField bell2;
    private static JTextField bell3;
    
    private static JTextField seven1;
    private static JTextField seven2;
    private static JTextField seven3;
    
    private static JTextField bar1;
    private static JTextField bar2;
    private static JTextField bar3;
    
    private static JTextField gold1;
    private static JTextField gold2;
    private static JTextField gold3;
    
    public AdjustOdds() {
        super("Adjust Odds");
        
        odds1 = Odds.getOdds1();
        odds2 = Odds.getOdds2();
        odds3 = Odds.getOdds3();
        
        setVisible(true);
        setSize(400, 300);
        setResizable(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocation(150, 150);
        
        JPanel grid = new JPanel();
        grid.setLayout(new GridLayout(6, 3));
        grid.add(new JLabel(""));
        grid.add(new JLabel("Slot 1"));
        grid.add(new JLabel("Slot 2"));
        grid.add(new JLabel("Slot 3"));
        
        grid.add(new JLabel("Cherry"));
        cherry1 = new JTextField("" + odds1[0]);
        cherry2 = new JTextField("" + odds2[0]);
        cherry3 = new JTextField("" + odds3[0]);
        grid.add(cherry1);
        grid.add(cherry2);
        grid.add(cherry3);
        
        grid.add(new JLabel("Bell"));
        bell1 = new JTextField("" + odds1[1]);
        bell2 = new JTextField("" + odds2[1]);
        bell3 = new JTextField("" + odds3[1]);
        grid.add(bell1);
        grid.add(bell2);
        grid.add(bell3);
        
        
        grid.add(new JLabel("Seven"));
        seven1 = new JTextField("" + odds1[2]);
        seven2 = new JTextField("" + odds2[2]);
        seven3 = new JTextField("" + odds3[2]);
        grid.add(seven1);
        grid.add(seven2);
        grid.add(seven3);
        
        
        grid.add(new JLabel("Bar"));
        bar1 = new JTextField("" + odds1[3]);
        bar2 = new JTextField("" + odds2[3]);
        bar3 = new JTextField(""  + odds3[3]);
        grid.add(bar1);
        grid.add(bar2);
        grid.add(bar3);
        
        grid.add(new JLabel("Gold Nugget"));
        gold1 = new JTextField("" + odds1[4]);
        gold2 = new JTextField("" + odds2[4]);
        gold3 = new JTextField("" + odds3[4]);
        grid.add(gold1);
        grid.add(gold2);
        grid.add(gold3);
        
        JPanel buttons = new JPanel();
        
        JButton save = new JButton("Save");
        save.addActionListener(this);
        JButton cancel = new JButton("Cancel");
        cancel.addActionListener(this);
        buttons.add(save);
        buttons.add(cancel);
        
        add(grid, BorderLayout.CENTER);
        add(buttons, BorderLayout.SOUTH);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String button = e.getActionCommand();
        if(button.equals("Save")) {
            
            try {
                Odds.setOdds1(Integer.parseInt(cherry1.getText()),
                     Integer.parseInt(bell1.getText()),
                     Integer.parseInt(seven1.getText()),
                     Integer.parseInt(bar1.getText()),
                     Integer.parseInt(gold1.getText()));
                
                Odds.setOdds2(Integer.parseInt(cherry1.getText()),
                     Integer.parseInt(bell2.getText()),
                     Integer.parseInt(seven2.getText()),
                     Integer.parseInt(bar2.getText()),
                     Integer.parseInt(gold2.getText()));
                
                Odds.setOdds3(Integer.parseInt(cherry3.getText()),
                     Integer.parseInt(bell3.getText()),
                     Integer.parseInt(seven3.getText()),
                     Integer.parseInt(bar3.getText()),
                     Integer.parseInt(gold3.getText()));

                Odds.oddsChanged();
                dispose();
            } catch (IOException error) {
                JOptionPane.showMessageDialog(this, "Odds must sum to 10!");
            }
        } else if (button.equals("Cancel")) {
            dispose();
        } else {
            // NOTHING
        }
    }
}
