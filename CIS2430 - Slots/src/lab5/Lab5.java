/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

/**
 *
 * @author James Wadsworth
 */
public class Lab5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Create user
        User user = new User(20);
        
        // Initialize odds
        Odds.updateElements(Odds.getOdds1(), Odds.getSlot1());
        Odds.updateElements(Odds.getOdds2(), Odds.getSlot2());
        Odds.updateElements(Odds.getOdds3(), Odds.getSlot3());
        GamePanel game = new GamePanel(user);
        game.setVisible(true);
    }
    
}
