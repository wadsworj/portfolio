/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

/**
 *
 * @author James Wadsworth
 */
public class User {
    private int credit;
    private int winnings;
    private final String name;
    private final String studentID;
    
    
    User (int credit) {
        this.credit = credit;
        this.winnings = 0;
        this.name = "James Wadsworth";
        this.studentID = "0813414";
        
    }
    
    public int getCredit() {
        return this.credit;
    }
    
    public void setCredit(int credit) {
        this.credit = credit;
    }
    public void addCredit(int credit) {
        this.credit += credit;
    }
    
    public String getName() {
        return this.name;
    }
    public String getStudentID() {
        return this.studentID;
    }
    public void upDateWinnings(String symbol) {
        addCredit(-3);
        this.winnings -= 3;
        
        if (symbol.equals("Cherry")) {
            this.winnings += 3;
            addCredit(3);
        } else if (symbol.equals("Bell")) {
            this.winnings += 6;
            addCredit(6);
        } else if (symbol.equals("Seven")) {
            this.winnings += 25;
            addCredit(25);
        } else if (symbol.equals("Bar")) {
            this.winnings += 100;
            addCredit(100);
        } else if (symbol.equals("Gold Nugget")) {
            this.winnings += 1000;
            addCredit(1000);
        } else {
            // Lost
        }
        
    }
    public int getWinnings() {
        return this.winnings;
    }
    
}
