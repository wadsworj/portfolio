/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author James Wadsworth
 */
public class Odds {
    private static Integer[] odds1 = {2, 2, 2, 2, 2};
    private static Integer[] odds2 = {2, 2, 2, 2, 2};
    private static Integer[] odds3 = {2, 2, 2, 2, 2};

    private static String[] slot1 = new String[10];
    private static String[] slot2 = new String[10];
    private static String[] slot3 = new String[10];
      
    public Odds() {
    }
    
    public static void updateElements(Integer[] odds, String[] slot) {
        int count = 0;
        System.out.println("odds.length" + odds.length);
        for (int i = 0; i < odds.length; i++) {
            System.out.println("odds[i]" + odds[i]);
            for (int j = 0; j < odds[i]; j++) {
                System.out.println(i);
                switch (i) {
                    // Cherry
                    case 0:
                        System.out.println("Count: " + count);
                        slot[count] = "Cherry";
                        break;
                    // Bell
                    case 1:
                        System.out.println("Count: " + count);
                        slot[count] = "Bell";
                        break;
                    // Seven
                    case 2:
                        System.out.println("Count: " + count);
                        slot[count] = "Seven";
                        break;
                    // Bar
                    case 3:
                        System.out.println("Count: " + count);
                        slot[count] = "Bar";
                        break;
                    // Gold Nugget
                    case 4:
                        System.out.println("Count: " + count);
                        slot[count] = "Gold Nugget";
                        break;
                }
                count++;
            }
        }
    }
    public static void oddsChanged() {
        clearSlots();
        updateElements(odds1, slot1);
        updateElements(odds2, slot2);
        updateElements(odds3, slot3);
    }
    
    public static String spinSlot(String[] slot){
        Random generate = new Random();
        int random = generate.nextInt(10);
        System.out.println("slot[random]: " + slot[0]);
        
        return slot[random];
    }
    
    private static void clearSlots() {
        slot1 = new String[10];
        slot2 = new String[10];
        slot3 = new String[10];
    }
    
    public static Integer[] getOdds1() {
        return odds1;
    }
    public static Integer[] getOdds2() {
        return odds2;
    }
    public static Integer[] getOdds3() {
        return odds3;
    }
    
    public static String[] getSlot1() {
        return slot1;
    }
    public static String[] getSlot2() {
        return slot2;
    }
    public static String[] getSlot3() {
        return slot3;
    }
    
    public static void setOdds1(int cherry, int bell, int seven, int bar, int gold) throws IOException {
        if ((cherry + bell + seven + bar + gold) != 10) {
            throw new IOException("does not sum");
            
        }
        odds1[0] = cherry;
        odds1[1] = bell;
        odds1[2] = seven;
        odds1[3] = bar;
        odds1[4] = gold;
    }
    public static void setOdds2(int cherry, int bell, int seven, int bar, int gold) {
        odds2[0] = cherry;
        odds2[1] = bell;
        odds2[2] = seven;
        odds2[3] = bar;
        odds2[4] = gold;
    }
    
    public static void setOdds3(int cherry, int bell, int seven, int bar, int gold) {
        odds3[0] = cherry;
        odds3[1] = bell;
        odds3[2] = seven;
        odds3[3] = bar;
        odds3[4] = gold;
    }
}
