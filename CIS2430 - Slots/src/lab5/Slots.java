/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.awt.*;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author James Wadsworth
 */
public class Slots extends JPanel {
    
    private static JLabel slotHolder1;
    private static JLabel slotHolder2;
    private static JLabel slotHolder3;
    
    private static JLabel label1;
    private static JLabel label2;
    
    public Slots () {
        ImageIcon slot1 = new ImageIcon("images/slot.png");
        ImageIcon labelImg = new ImageIcon("images/label.png");

        FlowLayout layout = new FlowLayout();
        this.setLayout(layout);

        this.setPreferredSize(new Dimension(550, 300));

        slotHolder1 = new JLabel(slot1);
        
        slotHolder2 = new JLabel(slot1);
        slotHolder3 = new JLabel(slot1);

        GridBagConstraints c = new GridBagConstraints();
        // c.insets = new Insets(0,00,0,0);
        add(slotHolder1, c);
        // c.insets = new Insets(-100,20,0,0);
        add(slotHolder2, c);
        // c.insets = new Insets(-100,20,0,0);
        add(slotHolder3, c);
        
        
        // Labels
        label1 = new JLabel(labelImg);
        label2 = new JLabel(labelImg);
        
        //Add text to labels
        label1.setText("Total Credits: " + GamePanel.getUser().getCredit());
        label1.setHorizontalTextPosition(JLabel.CENTER);
        label2.setText("Credits Won/Lost: " + GamePanel.getUser().getWinnings());
        label2.setHorizontalTextPosition(JLabel.CENTER);
        
        // c.insets = new Insets(0,980,0,0);
        add(label1, c);
        // c.insets = new Insets(0,-450,0,0);
        add(label2, c);
    }
    
    public static void updateTotalCredits() {
        label1.setText("Total Credits: " + GamePanel.getUser().getCredit());
        label1.setHorizontalTextPosition(JLabel.CENTER);
        label2.setText("Credits Won/Lost: " + GamePanel.getUser().getWinnings());
        label2.setHorizontalTextPosition(JLabel.CENTER);
    }
    
    public static void SpinEvent() throws OutOfCreditsException {
        
        if (GamePanel.getUser().getCredit() < 3) {
            throw new OutOfCreditsException("Not enough credits");
        }
        
        slotHolder1.setText(Odds.spinSlot(Odds.getSlot1()));
        slotHolder1.setHorizontalTextPosition(JLabel.CENTER);
        slotHolder2.setText(Odds.spinSlot(Odds.getSlot2()));
        slotHolder2.setHorizontalTextPosition(JLabel.CENTER);
        slotHolder3.setText(Odds.spinSlot(Odds.getSlot3()));
        slotHolder3.setHorizontalTextPosition(JLabel.CENTER);

        
        if (slotHolder1.getText().equals(slotHolder2.getText())
                && slotHolder1.getText().equals(slotHolder3.getText())) {
            // Winning event
            GamePanel.getUser().upDateWinnings(slotHolder1.getText());
            GamePanel.updateWins("win");
        } else {
            GamePanel.getUser().upDateWinnings("Lost");
            GamePanel.updateWins("lost");
        }
        
        updateTotalCredits();
    }
    
}
