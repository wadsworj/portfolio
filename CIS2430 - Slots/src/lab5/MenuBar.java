/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author James Wadsworth
 */
public final class MenuBar extends JMenuBar implements ActionListener {
    // Create Strings
    
    
    private static JMenu file;
    private static JMenu help;
    
    public MenuBar() {
        file = new JMenu("File");
        JMenuItem addCredits = new JMenuItem("Add Credits");
        JMenuItem adjustOdds = new JMenuItem("Adjust Odds");
        JMenuItem exit = new JMenuItem("Exit");
        
        addCredits.addActionListener(this);
        adjustOdds.addActionListener(this);
        exit.addActionListener(this);
        
        file.add(addCredits);
        file.add(adjustOdds);
        file.add(exit);
        
        help = new JMenu("Help");
        JMenuItem about = new JMenuItem("About");
        about.addActionListener(this);
        help.add(about);
        
        add(file);
        add(help);

    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String test = e.getActionCommand();
        
        if (test.equals("Add Credits")) {
            AddCredits credits = new AddCredits();
        } else if (test.equals("Adjust Odds")) {
            AdjustOdds odds = new AdjustOdds();
        } else if (test.equals("Exit")) {
            System.exit(0);
        } else if (test.equals("About")) {
            AboutWindow a = new AboutWindow();
        } else {
            // Nothing
        }
    }

}
