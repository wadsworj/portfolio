/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.awt.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author James Wadsworth
 */
public class Spin extends JPanel implements MouseListener {
    
    public Spin() {
        
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);

        this.setPreferredSize(new Dimension(200, 350));
        
        ImageIcon spinImg = new ImageIcon("images/spin.png");
        
        // Create Spin button
        GridBagConstraints c = new GridBagConstraints();
        JLabel spinButton = new JLabel(spinImg);
        c.insets = new Insets(0,0,0,0);
        
        // add text to spin button
        spinButton.setText("SPIN ME!");
        spinButton.setHorizontalTextPosition(JLabel.CENTER);
        spinButton.addMouseListener(this);
        
        add(spinButton);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        try {
            Slots.SpinEvent();
        } catch (OutOfCreditsException ex) {
            JOptionPane.showMessageDialog(this, "Not Enough Credits, Please Add Some More!");
            AddCredits credits = new AddCredits();
        }
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
