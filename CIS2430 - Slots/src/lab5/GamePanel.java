/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.awt.*;
import javax.swing.*;

/**
 *
 * @author James Wadsworth
 */
public class GamePanel extends JFrame {
    
    public static final int WIDTH = 805;
    public static final int HEIGHT = 622;
    private static User user;
    private static JLabel winloseLabel;
    private static int wins;
    private static int losses;
    
    public GamePanel(User player) {
        super("Game Panel");
        user = player;
        
        setSize(WIDTH, HEIGHT);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(50, 50);
        
        FlowLayout layout = new FlowLayout();
        this.setLayout(layout);
        
        // Slots Image
        ImageIcon winloseImg = new ImageIcon("images/winlose.png");
        
        // add(menu);
        setJMenuBar(new MenuBar());
        GridBagConstraints c = new GridBagConstraints();

        // Create Win-Lose Panel
        JPanel winlose = new JPanel(new GridBagLayout());
        winlose.setPreferredSize(new Dimension(WIDTH, 400));
        winloseLabel = new JLabel(winloseImg);
        setWinLoseLabel();
        c.insets = new Insets(-200,-140,0,0);
        winloseLabel.setHorizontalTextPosition(JLabel.CENTER);
        winlose.add(winloseLabel, c);
        
        add(new Slots());
        add(new Spin());
        add(winlose);
    }
    
    public static User getUser() {
        return user;
    }
    
    public static void setWinLoseLabel() {
        winloseLabel.setText("Wins: " + wins + " Losses: " + losses);
    }
    
    public static void updateWins (String result) {
        if (result.equals("win")) {
            wins++;
        } else {
            losses++;
        }
        setWinLoseLabel();
    }
    
}
