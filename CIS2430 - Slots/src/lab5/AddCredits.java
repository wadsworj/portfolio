/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author James Wadsworth
 */
public class AddCredits extends JFrame implements ActionListener {
    
    private static JTextField textBox;
    private static User user;
    
    public  AddCredits() {
        super("AddCredits");
        user = GamePanel.getUser();
        setVisible(true);
        setSize(400, 200);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocation(150, 150);
        
        JPanel grid = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0,0,0,0);
        grid.add(new JLabel("Credits "), c);
        
        textBox = new JTextField("");
        textBox.setPreferredSize(new Dimension(200,24));
        grid.add(textBox, c);
        
        JButton save = new JButton("Save");
        JButton cancel = new JButton("Cancel");
        save.addActionListener(this);
        cancel.addActionListener(this);
        grid.add(save, c);
        grid.add(cancel, c);
        
        add(grid, BorderLayout.CENTER);
        //add(buttons, BorderLayout.SOUTH);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    
        String button = e.getActionCommand();
        
        if (button.equals("Save")) {
            try {
                user.addCredit(Integer.parseInt(textBox.getText()));
            
                Slots.updateTotalCredits();
                dispose();
            } catch (NumberFormatException n) {
                JOptionPane.showMessageDialog(this, "Please Enter a Valid Number!");
                return;
            }
        } else if(button.equals("Cancel")) {
            dispose();
        } else {
            // Nothing
        }
    
    }


}
