/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polisim;

import java.util.ArrayList;

/**
 *
 * @author James Wadsworth
 */
public class Population {
    
    public static final int POPULATION_SIZE = 100;
    private static ArrayList<People> people = new ArrayList(100);
    
    private static int females = 0;
    private static int males = 0;
    private static int old = 0;
    private static int young = 0;
    private static int midaged = 0;
    
    
    private static int left;
    private static int leftCenter;
    private static int center;
    private static int rightCenter;
    private static int right;
    
    Population() {
        
        
        for (int i = 0; i < POPULATION_SIZE; i++) {
            people.add(new People());
            
            // age demographics
            if (people.get(i).getAge() == 0) {
                young++;
            }
            else if (people.get(i).getAge() == 1) {
                midaged++;
            }
            else {
                old++;
            }
            // number of females
            if (people.get(i).getGender() == 0) {
                males++;
            } else {
                females++;
            }
            
            // System.out.println(people.get(i).toString());
            // System.out.println("\n\n");
            
            if (people.get(i).getPoliticalLeaning() < 25)
                 left++;
            else if (people.get(i).getPoliticalLeaning() < 45)
                leftCenter++;
            else if (people.get(i).getPoliticalLeaning() < 55)
                center++;
            else if (people.get(i).getPoliticalLeaning() < 75)
                rightCenter++;
            else {
                right++;
            }
        }
        
        populationStats();
    }

    private void populationStats() {
        
        double RATIO = (double)POPULATION_SIZE;
        System.out.println("POPULATION STATS:");
        System.out.println("Percentage of males: "+ ((double)males/RATIO)*100 + "%");
        System.out.println("Percentage of females: "+ ((double)females/RATIO)*100 + "%");
        System.out.println("AGE BREAKDOWN\n\tyoung: "+ ((double)young/RATIO)*100 + "%"
                + "\n\tmidaged: "+ ((double)midaged/RATIO)*100 + "%"
                + "\n\told: "+ ((double)old/RATIO)*100 + "%");
        System.out.println("POLITICS BREAKDOWN\n\tleft: "+ ((double)left/RATIO)*100 + "%"
                + "\n\tcenter-left: "+ ((double)leftCenter/RATIO)*100 + "%"
                + "\n\tcenter: "+ ((double)center/RATIO)*100 + "%"
                + "\n\tcenter-right: "+ ((double)rightCenter/RATIO)*100 + "%"
                + "\n\tright: "+ ((double)right/RATIO)*100 + "%");
    }
    
    private void printMenu() {
        System.out.println("PoliSim Main Menu\n"
                           + "1. View Population Demographics");
        
    }

    public ArrayList<People> getPeople() {
        return people;
    }

    public void setPeople(ArrayList<People> people) {
        Population.people = people;
    }
    
    
}
