/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polisim;

import java.util.Scanner;

/**
 *
 * @author James Wadsworth
 */
public class PoliSim {
    
    public static void welcomeScreen() {
        
        System.out.println("Welcome to PoliSim, the political simulator.\n"
                + "Your job is take over the position of leader of the oposition\n"
                + "to the government and guide your party, and your country, to political,\n"
                + "economic, and military success.\n"
                + "\n"
                + "Will your country become the leader in innovation around the globe\n"
                + "or establish a new world empire?\n"
                + "\n"
                + "Your party, your country, and your globe are your sandbox.\n"
                + "Enjoy.\n");
    }
    
    
    public static void clearScreen() {
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }
    
    public static void printMenu() {
        System.out.println("\n\nPlease choose an option:\n"
                + "1. Start Game\n"
                + "2. About\n"
                + "3. Quit\n");
    }
    
    public static void gameMenu(){
        
    }
}
