package polisim;

import java.util.ArrayList;

public class Government {
    private int leader = -1;
    private Party left;
    private Party center;
    private Party right;
    private ArrayList<Member> members;
    
    Government(int userParty) {
        
        int party0 = 0;
        int party1 = 0;
        int party2 = 0;
        
        switch (userParty) {
            case 0:
                party0 = -10;
                party1 = 5;
                party2 = 0;
                break;
            case 1:
                party0 = 10;
                party1 = -10;
                party2 = 2;
                break;
            case 2:
                party0 = 0;
                party1 = 10;
                party2 = -5;
                break;
        }
        
        left = new Party("Labour", 0, party0, 32);
        center = new Party("Liberal", 0, party1, 50);
        right = new Party("Conservative", party2, 5, 68);
        members = new ArrayList<>(75);
        
    }
    
    

    public int getLeader() {
        return leader;
    }

    public void setLeader(int leader) {
        this.leader = leader;
    }

    public Party getLeft() {
        return left;
    }

    public void setLeft(Party left) {
        this.left = left;
    }

    public Party getParty(int party) {
        switch (party) {
            case 0:
                return left;
            case 1:
                return center;
            case 2:
                return right;
        }
        return null;
    }

    public void setCenter(Party center) {
        this.center = center;
    }

    public Party getRight() {
        return right;
    }

    public void setRight(Party right) {
        this.right = right;
    }

    public ArrayList<Member> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<Member> members) {
        this.members = members;
    }
    
}
