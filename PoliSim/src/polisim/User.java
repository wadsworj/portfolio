/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polisim;

/**
 *
 * @author James Wadsworth
 */
public class User extends Member {
    
    private int oppParty1;
    private int oppParty2;
    
    User(String name, int politics, int experience, int popularity, int party) {
        super(name, politics, experience, popularity, party);
        
        switch (party) {
            case 0:
                oppParty1 = 1;
                oppParty2 = 2;
                break;
            case 1:
                oppParty1 = 0;
                oppParty2 = 2;
                break;
            case 2:
                oppParty1 = 1;
                oppParty2 = 0;
                break;
        }
        
    }

    public int getOppParty1() {
        return oppParty1;
    }

    public void setOppParty1(int oppParty1) {
        this.oppParty1 = oppParty1;
    }

    public int getOppParty2() {
        return oppParty2;
    }

    public void setOppParty2(int oppParty2) {
        this.oppParty2 = oppParty2;
    }
    
    
    
    
}
