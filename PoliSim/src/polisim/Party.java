/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polisim;

/**
 *
 * @author lib_level
 */
public class Party {
    private String name;
    private int party;
    private int popularity;
    private int[] member;
    private int seats;
    private int politics;

    public Party(String name, int party, int popularity, int politics) {
        this.name = name;
        this.party = party;
        this.popularity = popularity;
        this.politics = politics;
        this.seats = 0;
    }

    public Party() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public int[] getMember() {
        return member;
    }

    public void setMember(int[] member) {
        this.member = member;
    }

    public int getPolitics() {
        return politics;
    }

    public void setPolitics(int politics) {
        this.politics = politics;
    }
    
    public void setSeats(int seats) {
        this.seats = seats;
    }
    
    public int getSeats() {
        return this.seats;
    }
}
