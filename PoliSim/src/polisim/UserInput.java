/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polisim;

import java.util.Scanner;

/**
 *
 * @author James Wadsworth
 */
public class UserInput {
    
    private static final Scanner keyboard = new Scanner(System.in);
    private static String userInput;
    
    // A method to recieve a string from the user
    public static String getString() {
        return keyboard.nextLine();
    }
    
    // A method to recieve a char as input
    public static char getChar() {
        String temp = keyboard.nextLine();
        return temp.charAt(0);
    }
    
    // A method to recieve an int as input
    public static int getInt() {
        
        boolean check;
        
        do {
            try {
                userInput = keyboard.nextLine();
                Integer.parseInt(userInput);
                check = true;
            } catch (NumberFormatException e) {
                System.out.println("Invalid input, please try again. Please enter a number.");
                check = false;
            }
        } while (check == false);
        
        return Integer.parseInt(userInput);
    }
    
    /* A method that prompts a user for a double value
        and tests for correct input */
    public static double getDouble() {
        
        boolean check;
        
        do {
            try {
                userInput = keyboard.nextLine();
                Double.parseDouble(userInput);
                check = true;
            } catch (NumberFormatException e) {
                System.out.println("Invalid input, please try again. Please enter a number.");
                check = false;
            }
        } while (check == false);
        
        return Double.parseDouble(userInput);
    }
}
