/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polisim;

import java.util.Random;

/**
 *
 * @author James Wadsworth
 */
public class People {
    private int age;
    private int gender;
    private int wealth;
    private int religion;
    private int location;
    private int politicalLeaning;
    
    // Default constructor
    People() {
        
        // Random number generation
        Random random = new Random();
        
        int leaningModifier = (random.nextInt((3) + 1));
        this.politicalLeaning = 50;
        
        // How old is this person
        int randomNum = random.nextInt((100 - 1) + 1) + 1;
        
        /* 30% chance the person is young
         * 40% chance the person is midaged
         * 30% chance the person is a senior
         */
        if (randomNum < 30) {
            this.age = 0;       // Young
            this.politicalLeaning += leaningModifier*(-8 - leaningModifier);
        } else if (randomNum < 70) {
            this.age = 1;       // Mid Aged
        } else {
            this.age = 2;       // Senior
            this.politicalLeaning += leaningModifier*(4 + leaningModifier);
        }
        // Male or Female
        this.gender = random.nextInt((1 - 0) + 1) + 0;
        
        // Wealth
        randomNum = random.nextInt((100 - 1) + 1) + 1;
        if (randomNum < 30) {
            this.wealth = 0;        // Low income person
            this.politicalLeaning += leaningModifier*(-8 - leaningModifier);
        } else if (randomNum < 70) {
            this.wealth = 1;        // Middle income person
        } else {
            this.wealth = 2;        // High income person
            this.politicalLeaning += leaningModifier*(5 + leaningModifier);
        }
        // Religious or not
        randomNum = random.nextInt((10 - 1) + 1) + 1;
        if (randomNum < 4) {
            this.religion = 1;        // Is a religious person
            this.politicalLeaning += leaningModifier*(8 + leaningModifier);
        } else {
            this.religion = 0;        // Is not a religious person
            this.politicalLeaning += leaningModifier*(-2 - leaningModifier);
        }
        /* Rural vs urban
         * 30% chance of being rural
         * 70% chance of being urban
         */
        randomNum = random.nextInt((10 - 1) + 1) + 1;
        if (randomNum < 4) {
            this.location = 1;        // Is rural
            this.politicalLeaning += leaningModifier*(10 + leaningModifier);
        } else {
            this.location = 0;        // Is urban
            this.politicalLeaning += leaningModifier*(-3 - leaningModifier);
        }
    }

    // Accessors
    public int getAge() {
        return this.age;
    }

    public int getGender() {
        return this.gender;
    }

    public int getWealth() {
        return this.wealth;
    }

    public int getReligion() {
        return this.religion;
    }

    public int getPoliticalLeaning() {
        return this.politicalLeaning;
    }

    // Mutators
    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public void setWealth(int wealth) {
        this.wealth = wealth;
    }

    public void setReligion(int religion) {
        this.religion = religion;
    }

    public void setPoliticalLeaning(int politicalLeaning) {
        this.politicalLeaning = politicalLeaning;
    }

    @Override
    public String toString() {
        String wealthS = null;
        String ageS = null;
        String genderS = null;
        String religionS = null;
        String politicS = null;
        String locationS = null;
                
        if (this.wealth == 0)
            wealthS = "Low Income";
        else if (this.wealth == 1)
            wealthS = "Middle Income";
        else if (this.wealth == 2)
            wealthS = "High Income";
        
        if (this.age == 0)
            ageS = "Young";
        else if (this.age == 1)
            ageS = "Middle Age";
        else if (this.age == 2)
            ageS = "Senior";
        
        if (this.gender == 0)
            genderS = "Male";
        else if (this.gender == 1)
            genderS = "Female";
        
        if (this.religion == 0)
            religionS = "No Religion";
        else if (this.religion == 1)
            religionS = "Religious";
        
        if (this.location == 1)
            locationS = "Rural";
        else if (this.location == 0)
            locationS = "Urban";
        
        if (this.politicalLeaning < 35)
            politicS = "Left";
        else if (this.politicalLeaning < 65)
            politicS = "Center";
        else {
            politicS = "Right";
        }
        
        return "Gender: " + genderS +
                "\nAge: " + ageS +
                "\nWealth: " + wealthS +
                "\nReligion: " + religionS +
                "\nLocation: " + locationS +
                "\nPolitical Score: " + politicalLeaning +
                "\nPolitical Leaning: " + politicS;
    }


    
    
    
}
