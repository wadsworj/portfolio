/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polisim;

import java.util.Random;

/**
 *
 * @author James Wadsworth
 */
public final class Country {

    private  Population population;
    private  Government government;
    private final int NUMBER_OF_VOTES = 150;

    Country(int userParty) {
        population = new Population();
        government = new Government(userParty);
        election();
    }
    
    
    public void election() {
        int leftVotes = 0;
        int centerVotes = 0;
        int rightVotes = 0;
        
        int leftSeats = 0;
        int centerSeats = 0;
        int rightSeats = 0;
        
        int votes;
        
        Random random = new Random();
        
        for (int i = 0; i < Population.POPULATION_SIZE; i++) {
            int dif1, dif2, dif3;
            votes = 75;
            People temp;
            temp = population.getPeople().get(i);
            dif1 = Math.abs(government.getLeft().getPolitics() - temp.getPoliticalLeaning());
            dif2 = Math.abs(government.getParty(1).getPolitics() - temp.getPoliticalLeaning());
            dif3 = Math.abs(government.getRight().getPolitics() - temp.getPoliticalLeaning());
            dif1 = dif1 - government.getLeft().getPopularity();
            dif2 = dif2 - government.getParty(1).getPopularity();
            dif3 = dif3 - government.getRight().getPopularity();
            
            if (dif1 < dif2 && dif1 < dif3) {
                int modifier = (random.nextInt(7));
                centerVotes += modifier;
                votes = votes - modifier;
                modifier = (random.nextInt(3));
                rightVotes += modifier;
                votes = votes - modifier;
                leftVotes += votes;
                
            } else if (dif2 < dif3 && dif2 < dif3) {
                int modifier = (random.nextInt(5));
                leftVotes += modifier;
                votes = votes - modifier;
                modifier = (random.nextInt(5));
                rightVotes += modifier;
                votes = votes - modifier;
                centerVotes += votes;
                
            } else {
                int modifier = (random.nextInt(7));
                centerVotes += modifier;
                votes = votes - modifier;
                modifier = (random.nextInt(3));
                leftVotes += modifier;
                votes = votes - modifier;
                rightVotes += votes;
            }
            
        }
        
        leftSeats = (int)(((double)leftVotes/7500) * 75);
        centerSeats = (int)(((double)centerVotes/7500) * 75);
        rightSeats = (int)(((double)rightVotes/7500) * 75);
        
        while ((leftSeats + centerSeats + rightSeats) < 75) {
            if ((leftSeats > centerSeats) && (leftSeats > rightSeats))
                leftSeats++;
            else if ((leftSeats < centerSeats) && (centerSeats > rightSeats))
                centerSeats++;
            else
                rightSeats++;
        }
        int total = leftSeats + centerSeats + rightSeats;
        System.out.println(total);
        
        System.out.println(leftSeats);
        System.out.println(centerSeats);
        System.out.println(rightSeats);
        
        System.out.println("Labour votes: " + leftVotes + " : " + ((double)leftVotes/7500*100) + "%");
        System.out.println("Liberal votes: " + centerVotes + " : " + ((double)centerVotes/7500*100) + "%");
        System.out.println("Conservative votes: " + rightVotes + " : " + ((double)rightVotes/7500*100) + "%");
        
        
        government.getParty(0).setSeats(leftSeats);
        government.getParty(1).setSeats(centerSeats);
        government.getParty(2).setSeats(rightSeats);
    }
    
    public Population getPopulation() {
        return population;
    }

    public void setPopulation(Population population) {
        this.population = population;
    }

    public Government getGovernment() {
        return government;
    }

    public void setGovernment(Government government) {
        this.government = government;
    }
    

    
}
