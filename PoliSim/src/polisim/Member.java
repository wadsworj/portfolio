/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polisim;

/**
 *
 * @author lib_level
 */
public class Member {
    private String name;
    private int politics;
    private int experience;
    private int popularity;
    private int party;
    
    Member(String name, int politics, int experience, int popularity, int party) {
        this.name = name;
        this.politics = politics;
        this.experience = experience;
        this.popularity = popularity;
        this.party = party;
    }

    // mutators accessors
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPolitics() {
        return politics;
    }

    public void setPolitics(int politics) {
        this.politics = politics;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public int getParty() {
        return party;
    }
    
    public String getPartyString() {
        switch (this.party) {
            case 0:
                return "Labour";
            case 1:
                return "Liberal";
            case 2:
                return "Conservative";
        }
        return null;
    }

    public void setParty(int party) {
        this.party = party;
    }
    
    public String toString() {
        return "Name: " + this.name
                + "\nParty: " + this.party
                + "\nPolitics: " + this.politics;
    }
    
}
