==== Information ====
Author: James Wadsworth
Student ID: 0813414
Date: November 28th, 2014


GENERAL PROBLEM TRYING TO SOLVE
-------------------------------
My DayPlanner program is created to store and display life events that take place
at School, Home, and Other. The program allows users to enter activities and search
for previously entered activities.

The program allows the user to do this through a GUI.

The class Activity was code provided by the prof of 2430 and I do not take any
credit for those sections, only the methods and additions I have made



EXTRA FEATURES
--------------
-GUI layout implements almost exactly as shown in assignment description
(it looks really good in my humble opinion, worth a bonus?)
-Allows user to load multiple files by the menu command "Load File"
-Confirm User Wants to Quit
-Display All: Added a new panel which prints all current activities
-Buttons added to welcome screen
-When an invalid time is inputted, only the time field is cleared, title
and other fields are saved.


EXCEPTION HANDLING
------------------
AddLeftPanel.enterActivity():
		handles exceptions thrown when creating a new
		object, such as no title, invalid time format, and
		time out of range (ex. month > 12)
		
DayPlanner.addActivity():
		throws SizeLimitExceededException if there is no
		more room for an activity.
Interface.loadFile():
		handles exceptions thrown when creating loading
		a new file. If file is not found, prints error message

ASSUMPTIONS AND LIMITATIONS
---------------------------
Limitations from Assignment 1 and 2 remain
JFrame is not resizeable
Keywords are not resizeable
Does not test for overlapping time
Does not test for start time coming before end time
21 Java files. This is not a limitation but not organized great.
	Breakdown of files into groups
	-GUI
		-Assignment3 (Main File)
		-Interface (Main JFrame)
		-SearchPanel (Main Search JPanel)
			-SearchLeftPanel
			-SearchRightPanel
			-SearchBottomPanel
		-AddPanel (Main Add JPanel)
			-AddLeftPanel
			-AddRightPanel
			-AddBottomPanel
		-WelcomePanel (JPanel)
		-DisplayALL (JPanel)
		-MenuBar (JMenuBar)
		-InvalidTimeException (custom exception)
			
USER GUIDE
----------
The easiest way to run program is to load the project into NetBeans.
It is also possible to run from the command line (using javac and java commands).


**********************
*----TESTING PLAN----*
**********************

TEST CASE 1-ACTIVITY INPUT
--------------------------
Test that the program can handle basic activity input

-----------------
Test case:
-----------------
	Type: School
	Title: Java Exam
	Starting Time: 2014, 12, 02, 12, 30
	Ending Time: 2014, 12, 02, 13, 30
	Comment: This should be easy
	
-----------------------------
Observations/Adjustments:
-----------------------------
The activity was saved and displayed when asked to print.
The add activity did not reset though and no confirmation
was displayed in the "messages" section. I have updated the
program to clear all input fields once an addition/search
has taken place and a message is now printed in the messages
section after a successful addition.

File saving from assignment 2 is not working. Might not
have time to fix this.


TEST CASE 2-SEARCH ATTEMPT
--------------------------
Test that the program can handle multiple cases of
search and multiple inputs.

-----------------
Test case:
-----------------
Multiple combinations of trying to find the above
activity.

Example: Search for "Java Exam" by entering the type and time
but not the title.
-----------------------------
Observations/Adjustments:
-----------------------------
Search is not searching by Type. Update this.
No message stating nothing found, added.
All items are not displayed when no input given, fixed.
Location text box was showing up in search, removed.
