/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */
package assignment3;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * AddRightPanel with buttons to control behaviour of JPanel.
 * 
 * @author James Wadsworth
 */
public class AddRightPanel extends JPanel implements MouseListener{
    // Right Panel
    AddRightPanel() {
        setBackground(Color.WHITE);
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        // Image for JLabel button
        ImageIcon resetButton = new ImageIcon("images/reset_button.png");
        
        // reset button
        JLabel reset = new JLabel(resetButton);
        reset.setText("Reset");
        reset.setHorizontalTextPosition(JLabel.CENTER);
        reset.addMouseListener(this);
        
        // enter button
        JLabel enter = new JLabel(resetButton);
        enter.setText("Enter");
        enter.setHorizontalTextPosition(JLabel.CENTER);
        enter.addMouseListener(this);
        
        // Add elements in center of panel
        reset.setAlignmentX(Component.CENTER_ALIGNMENT);
        enter.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        c.insets = new Insets(-170,0,0,0);
        add(reset, c);
        c.insets = new Insets(30,-72,0,0);
        add(enter, c);
    }

    /**
     * Listen for when user clicks a button
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        JLabel label = (JLabel)e.getSource();
        if(label.getText() == "Reset") {
            AddLeftPanel.clear();                   // Clear inputs
            AddBottomPanel.setMessageText("");      // Clear any warning messages
        }
        if(label.getText() == "Enter") {
            AddLeftPanel.enterActivity();
        }
    }

    /**
     * Unused
     * @param e
     */
    @Override
    public void mousePressed(MouseEvent e) {
    }

    /**
     * Unused
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Unused
     * @param e
     */
    @Override
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Unused
     * @param e
     */
    @Override
    public void mouseExited(MouseEvent e) {
    }
}
