/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */
package assignment3;

import java.awt.*;
import java.util.ArrayList;
import javax.swing.*;

/**
 *
 * @author James Wadsworth
 */
public class DisplayAll extends JPanel {
    
    private static ArrayList<Activity> activities;
    private static JTextArea list;
    
    DisplayAll () {
        setBackground(Color.WHITE);
        FlowLayout layout = new FlowLayout();
        Font font = new Font("Verdana", Font.PLAIN, 14);
        
        JTextArea title = new JTextArea();
        title.setText("\tAll Activities:\t\t\t\t\t\t");
        title.setFont(font);
        
        list = new JTextArea();
        list.setEditable(false);
        list.setPreferredSize(new Dimension(450, 530));
        
        JScrollPane scroll = new JScrollPane(list);
        scroll.setPreferredSize(new Dimension(480, 445));
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        
        // Add elements to JPanel
        add(title);
        add(scroll);
        
        
    }
    
    /**
     * 
     */
    public static void updateList() {
        System.out.println("made it here!");
        activities = DayPlanner.getActivities();
        
        for (int i = 0; i < activities.size(); i++) {
            System.out.println(activities.get(i).toString());
        }
        
        list.setText("");
        
        for (int i = 0; i < activities.size(); i++) {
                list.setText(list.getText() + "\n\n" + activities.get(i).toString());
        }
        list.revalidate();
        list.repaint();
    }
    
    /**
     * 
     */
    public void getList() {
        
    }
    
}
