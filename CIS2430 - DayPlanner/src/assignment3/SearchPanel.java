/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */
package assignment3;

import java.awt.*;
import javax.swing.*;

/**
 * SearchPanel manages the search display.
 * 
 * The JPanel is made up of three child JPanels, SearchLeftPanel,
 * SearchRightPanel, and SearchBottomPanel
 * 
 * @author James Wadsworth
 */
public class SearchPanel extends JPanel {
    
    private static final int LEFT_WIDTH = 360;
    private static final int LEFT_HEIGHT = 250;
    
    SearchPanel() {
        setBackground(Color.WHITE);
        FlowLayout layout = new FlowLayout();
        setLayout(layout);

        SearchRightPanel right = new SearchRightPanel();
        SearchLeftPanel left = new SearchLeftPanel();
        SearchBottomPanel bottom = new SearchBottomPanel();

        // Set Sizes of JPanels
        left.setPreferredSize(new Dimension(LEFT_WIDTH, LEFT_HEIGHT));
        right.setPreferredSize(new Dimension(130, LEFT_HEIGHT));
        bottom.setPreferredSize(new Dimension(510, 350));
        
        // Add each JPanel to the SearchPanel
        add(left);
        add(right);
        add(bottom);
        setVisible(true);
    }
}
