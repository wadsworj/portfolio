
package assignment3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;
import javax.naming.SizeLimitExceededException;

/** DayPlanner class
 *
 * @author Fei Song
 *
 * A class that adds and searches activities.
 *
 */

public class DayPlanner {

    /**
     * 
     */
    public static final int MAX_SIZE = 25;
    /**
     * 
     */
    public static final String[] types = new String[]{"home", "school", "other", "h", "s", "o"};
    private static ArrayList<Activity> activities = new ArrayList<Activity>(MAX_SIZE);
    private static int activitySize; 
    
    /* Hash map for searching */
    private static HashMap<String, Integer> hashMap = new HashMap<String, Integer>(activities.size());
    
    /**
     * Constructor for day planner
     */
    public DayPlanner() {
            activities = new ArrayList<Activity>(MAX_SIZE);
            activitySize = 0;
    }
    
    /*
     * Create a time object for the valid input
     * 
     */
    /**
     * 
     * @param line
     * @return
     */
    public static Time getTime(String line) {
            String[] tokens = line.split("[ ,\n]+");
            if (tokens.length != 5)
                    return null;
            for (int i = 0; i < 5; i++ )
                if (!tokens[i].matches("[-+]?[0-9]+"))
                    return null;
            int year = Integer.parseInt(tokens[0]);
            int month = Integer.parseInt(tokens[1]);
            int day = Integer.parseInt(tokens[2]);
            int hour = Integer.parseInt(tokens[3]);
            int minute = Integer.parseInt(tokens[4]);
            if (Time.timeOK(year, month, day, hour, minute))
                    return new Time(year, month, day, hour, minute);
            else
                    return null;
    }
    
    /**
     * 
     * @return
     */
    public static ArrayList<Activity> getActivities() {
        return activities;
    }

    /*
     * Add a valid home activity
     */
    /**
     * 
     * @param activity
     * @throws SizeLimitExceededException
     */
    public static void addActivity(Activity activity) throws SizeLimitExceededException {
        String title = activity.getTitle();
        StringTokenizer token = new StringTokenizer(title, " ");
        if (activitySize == MAX_SIZE)
                throw new SizeLimitExceededException("No more room for activites");
        else	
                activities.add(activity);
                activitySize++;

        // Hash map
        while (token.hasMoreTokens()) {
            hashMap.put(token.nextToken() , (activities.size() - 1));
        }
    }

    /** 
     * Create an activity from the input and add it to the appropriate list
     * @param input 
     */
    public void addActivity(Scanner input) {
            String type = "";
            do {
                    System.out.print( "Enter an activity type (home, school, or other)> " );
                    type = input.nextLine();
            } while (!matchedKeyword(type, types));

            System.out.print("Enter a title> ");
            String title = input.nextLine();

            Time startingTime = null;
            do {
                    System.out.print( "Enter a starting time (year, month, day, hour, minute)> " );
                    startingTime = getTime( input.nextLine() );
            } while (startingTime == null);

            Time endingTime = null;
            do {
                    System.out.print( "Enter an ending time (year, month, day, hour, minute)> " );
                    endingTime = getTime( input.nextLine() );
            } while (endingTime == null);

            String location = "";
            if( type.equalsIgnoreCase("other") || type.equalsIgnoreCase("o") ) {
                    System.out.print( "Enter a location> " );
                    location = input.nextLine();
            }

            System.out.print( "Enter a line of comment> " );
            String comment = input.nextLine();
            
            try {
                if (type.equalsIgnoreCase("home") || type.equalsIgnoreCase("h"))
                        addActivity(new HomeActivity(title, startingTime, endingTime, comment));
                else if (type.equalsIgnoreCase("school") || type.equalsIgnoreCase("s"))
                        addActivity(new SchoolActivity(title, startingTime, endingTime, comment));
                else
                        addActivity(new OtherActivity(title, startingTime, endingTime, location, comment));
            } catch (SizeLimitExceededException e) {
                System.out.println("failure to add activity");
            } finally {
                // Something here
            }
    }

    /* 
     * Check if a keyword is on a list of tokens
     */
    private boolean matchedKeyword(String keyword, String[] tokens) {
            for (int i = 0; i < tokens.length; i++) {
                    if (keyword.equalsIgnoreCase(tokens[i]))
                            return true;
            }
            return false;
    }

    /*
     * Check if all keywords are in a string 
     
    private boolean matchedKeywords(String input, String title) {
            String[] tokens = title.split( "[ ,\n]+" );
            for (int i = 0; i < keywords.length; i++) 
                    if (!matchedKeyword(keywords[i], tokens))
                            return false;
            return true;
    }
    */
    /*
     * Search for all home activities that satisfy a search request
     * Written by Fei Song, modified by James Wadswoth for A3
     */
    /**
     * 
     * @param type
     * @param title
     * @param startingTime
     * @param endingTime
     */
    public static void searchActivities(String type, String title, Time startingTime, Time endingTime) {
        boolean found = false;
        String objectType = null;
        
            for (int i = 0; i < activitySize; i++) {
                if (activities.get(i) instanceof HomeActivity) {
                    objectType = "Home";
                }
                else if (activities.get(i) instanceof OtherActivity) {
                    objectType = "Other";
                }
                else {
                    objectType = "School";
                }
                
                    objectType = "School";
                    if ((type.equals("") || (objectType.equals(type))) &&
                        (startingTime == null || activities.get(i).getStartingTime().compareTo(startingTime) >= 0) &&
                        (endingTime == null || activities.get(i).getEndingTime().compareTo(endingTime) <= 0) &&
                        (title.equals("") || (title.equals(activities.get(i).getTitle())))) {
                        System.out.println("testing...");
                            SearchBottomPanel.addMessageText(activities.get(i).toString() + "\n\n"); 
                            found = true;
                    }
            }
            if (!found) {
                SearchBottomPanel.addMessageText("No activity was found. Please try again."); 
            }
    }
    /**
     * Gather a search request and find the matched activities in the related list(s)
     * @param fileName 
     * @throws FileNotFoundException 
     */ 


    public void openFile(String fileName) throws FileNotFoundException {
        Scanner file = null;
        ArrayList<String> input = new ArrayList<String>(5);
        int count = 0;
        Time startTime = null;
        Time endTime = null;
        // Check that file was opened, else throw exception
        try {
            file = new Scanner(new FileInputStream(fileName));
        } catch (FileNotFoundException e) {
            System.out.println("File was not found");
            throw e;
        }
        // delimitor
        file.useDelimiter("[\",\n]+");
        String temp = null;
        // check that there is another token
        while (file.hasNext()) {
            file.useDelimiter("[\"\\n]");
            
            // create a temp string
            temp = file.next();
            switch(temp.charAt(0)){
                // when case e, next token is ending time
                case 'e':
                    temp = file.next();
                    endTime = getTime(temp);
                    if (file.hasNext()) {
                        file.next();
                    }
                    break;
                // when case s, next token is starting time
                case 's':
                    temp = file.next();
                    startTime = getTime(temp);
                    if (file.hasNext()) {
                        file.next();
                    }
                    break;
                /* when case l, next token is location, decrese count because 
                    this only happens for other activity */
                case 'l':
                    count = count - 1;
                case 't':
                case 'c':
                    temp = file.next();
                    
                    input.add(temp);
                    if (file.hasNext()) {
                        file.next();
                    }
            }
            // each time we parse a line, increment
            count++;
            
            // when count == 5 activity is ready to be created
            if (count == 5) {
                System.out.println(input.get(0));
                try {
                    if (input.get(0).equals("school")) {
                        System.out.println("school1");
                        SchoolActivity newActivity = new SchoolActivity(input.get(1), startTime, endTime,
                                input.get(2));
                        addActivity(newActivity);
                    }
                    if (input.get(0).equals("other")) {
                        System.out.println("other1");
                        OtherActivity newActivity = new OtherActivity(input.get(1), startTime, endTime,
                                input.get(2), input.get(3));
                        addActivity(newActivity);
                    }
                    if (input.get(0).equals("home")) {
                        System.out.println("home1");
                        HomeActivity newActivity = new HomeActivity(input.get(1), startTime, endTime,
                                input.get(2));
                        addActivity(newActivity);
                    }
                } catch (SizeLimitExceededException e) {
                    System.out.println("failure to add activity");
                } finally {
                     // Something here
                }
                // clear our temp array
                input.clear();
                if (file.hasNext()) {
                        file.next();
                }
                // reset count to zero
                count = 0;
            }
                
                
            }
        // close file after reading from it
        file.close();
    }
    
    /**
     * print all current activities in program
     */
    public static void printActivities () {
        for (int i = 0; i < activities.size(); i++) {
            System.out.println(activities.get(i).toString());
        }
    }
    
    /*
    public void searchHashMap(String key) {
        
        StringTokenizer token = new StringTokenizer(key, " ");
        
        
        int numOfKeys = token.countTokens();
        
        
        for (int i = 0; i < numOfKeys; i++) {
            ArrayList<Integer> set = new ArrayList<Integer>();
        }
        
        for (int i = 0; i < numOfKeys; i++) {
           set.add(hashMap.get(token.nextToken()));
           while (token.hasMoreTokens()) {
               token.nextToken();
           }
       }
        
        
    }
    */
}
