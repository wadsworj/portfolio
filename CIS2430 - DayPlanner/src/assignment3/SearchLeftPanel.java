/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */
package assignment3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * SearchLeftPanel creates the JPanel on the left of the SEARCH panel.
 * This panel manages the form for adding an activity.
 * 
 * Each section of the form is added by using a JPanel with text
 * and elements added to each JPanel.
 * 
 * @author James Wadsworth
 */
public class SearchLeftPanel extends JPanel {
    
    private static final int LEFT_WIDTH = 360;
    private static final int LEFT_HEIGHT = 320;
    
    private static JTextField titleBox;
    private static JTextField startingBox;
    private static JTextField endingBox;
    private static JTextField commentBox;
    
    private static JComboBox typeBox;
    private static final String[] typeStrings = {"", "School", "Home", "Other"};
    
    SearchLeftPanel() {
        setBackground(Color.WHITE);
        setLayout(new FlowLayout());
        
        
        JTextArea text1 = new JTextArea();
        JTextArea text2 = new JTextArea();
        JTextArea text3 = new JTextArea();
        JTextArea text4 = new JTextArea();
        JTextArea text5 = new JTextArea();
        
        text1.setEditable(false);
        text2.setEditable(false);
        text3.setEditable(false);
        text4.setEditable(false);
        text5.setEditable(false);
        
        Font font = new Font("Verdana", Font.PLAIN, 12);
        text1.setFont(font);
        text2.setFont(font);
        text3.setFont(font);
        text4.setFont(font);
        text5.setFont(font);

        
        text1.setText("Searching activities:");
        text1.setPreferredSize(new Dimension( 300, 24 ));
        text2.setText("     Type: ");
        text3.setText("     Title: ");
        text4.setText(" Starting time (year, month, day, hour, minute):");
        text5.setText(" Ending time (year, month, day, hour, minute): ");
       
        
        // JComboBoxes
        typeBox = new JComboBox(typeStrings);
        typeBox.setSelectedIndex(0);
        typeBox.setPreferredSize(new Dimension( 200, 24 ));
                
        // JTextFields
        titleBox  = new JTextField();
        titleBox.setText("");
        titleBox.setPreferredSize(new Dimension( 250, 24 ));
        
        startingBox  = new JTextField();
        startingBox.setText("");
        startingBox.setPreferredSize(new Dimension( 250, 24 ));
        
        endingBox  = new JTextField();
        endingBox.setText("");
        endingBox.setPreferredSize(new Dimension( 250, 24 ));

        commentBox  = new JTextField();
        commentBox.setText("");
        commentBox.setPreferredSize(new Dimension( 250, 24 ));
        
        // JPanel Instances
        JPanel addingActivity = new JPanel();
        addingActivity.setLayout(new FlowLayout(FlowLayout.LEFT));
        addingActivity.setPreferredSize(new Dimension(LEFT_WIDTH, 25));
        addingActivity.setBackground(Color.WHITE);
        
        JPanel type = new JPanel();
        type.setPreferredSize(new Dimension(LEFT_WIDTH, 35));
        type.setLayout(new FlowLayout(FlowLayout.LEFT));
        type.setBackground(Color.WHITE);
        
        JPanel title = new JPanel();
        title.setPreferredSize(new Dimension(LEFT_WIDTH, 35));
        title.setLayout(new FlowLayout(FlowLayout.LEFT));
        title.setBackground(Color.WHITE);
        
        JPanel time = new JPanel();
        time.setPreferredSize(new Dimension(LEFT_WIDTH, 105));
        time.setLayout(new FlowLayout(FlowLayout.LEFT));
        time.setBackground(Color.WHITE);
        
        
        JPanel comment = new JPanel();
        comment.setPreferredSize(new Dimension(LEFT_WIDTH, 35));
        comment.setLayout(new FlowLayout(FlowLayout.LEFT));
        comment.setBackground(Color.WHITE);
        
        
        addingActivity.add(text1);

        type.add(text2);
        type.add(typeBox);
        
        title.add(text3);
        title.add(titleBox);
        
        time.add(text4);
        time.add(startingBox);
        time.add(text5);
        time.add(endingBox);

        add(addingActivity);
        add(type);
        add(title);
        add(time);
    }
    
    /**
     * Clear all text that has been entered
     */
    public static void clear() {
        titleBox.setText("");
        startingBox.setText("");
        endingBox.setText("");
        commentBox.setText("");
    }
    
    /**
     * Clear the time inputs
     */
    private static void clearTime() {
        startingBox.setText("");
        endingBox.setText("");
    }
    
    /**
     * A function to save all inputted data and search
     */
    public static void searchActivity(){
        Time startingTime;
        Time endingTime;
        try {
            if (!startingBox.getText().equals("")) {
                startingTime = new Time(startingBox.getText());
            } else {
                startingTime = null;
            }
            if (!endingBox.getText().equals("")) {
                endingTime = new Time(startingBox.getText());
            } else {
                endingTime = null;
            }
            String type = typeStrings[typeBox.getSelectedIndex()];
            DayPlanner.searchActivities(type, titleBox.getText(), startingTime, endingTime);
            clear();
            
        } catch (InvalidTimeException e) {
            AddBottomPanel.setMessageText(e.getMessage() +
                    "\nInvalid Time Format! Please try again");
            clearTime();
        }
    }
}
