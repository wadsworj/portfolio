/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */
package assignment3;

import java.io.FileNotFoundException;
import javax.swing.JFrame;
import java.awt.*;
import javax.swing.JOptionPane;

/**
 * Class to create the main JFrame for the program
 * @author lib_level
 */
public class Interface {
    private static final int WIDTH = 535;
    private static final int HEIGHT = 600;
    private static WelcomePanel welcome;
    private static AddPanel addPanel;
    private static SearchPanel searchPanel;
    private static DisplayAll displayPanel;
    
    private static String currentPanel;
    private static JFrame frame;
    
    
    /**
     * 
     */
    public Interface() {
        frame = new JFrame("Day Planner");
        frame.setSize(WIDTH, HEIGHT);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setLocation(50, 50);
        
        FlowLayout layout = new FlowLayout();
        frame.setLayout(layout);
        frame.setJMenuBar(new MenuBar());
        addPanel = new AddPanel();
        welcome = new WelcomePanel();
        searchPanel = new SearchPanel();
        displayPanel = new DisplayAll();
        
        welcome.setPreferredSize(new Dimension(780, 530));
        addPanel.setPreferredSize(new Dimension(780, 530));
        searchPanel.setPreferredSize(new Dimension(780, 530));
        displayPanel.setPreferredSize(new Dimension(780, 530));
        
        // Initialize the panel to show the welcome screen
        addPanel("welcome");
       
        frame.setVisible(true);
    }
    
    /**
     * 
     * @param string
     */
    public static void removeCurrentPanel(String string) {
        
        if (string.equals("welcome")) {
            frame.getContentPane().remove(getWelcome());
        } else if (string.equals("add")) {
            frame.getContentPane().remove(getAddPanel());
        } else if (string.equals("search")) {
            frame.getContentPane().remove(getSearchPanel());
        } else if (string.equals("display")) {
            frame.getContentPane().remove(getDisplayPanel());
        } else {
            
        }
    }
    
    /**
     * Choose which panel to display
     * @param string
     */
    public static void addPanel(String string) {
        System.out.println("String: " + string);
        if (string.equals("welcome")) {
            
            frame.add(getWelcome());
            setCurrentPanel("welcome");
            frame.getContentPane().invalidate();
            frame.getContentPane().validate();
            //frame.repaint();
            
        } else if (string.equals("add")) {
            
            frame.add(getAddPanel());
            setCurrentPanel("add");
            frame.getContentPane().invalidate();
            frame.getContentPane().validate();
            //frame.repaint();
            
        } else if (string.equals("search")) {
            frame.add(getSearchPanel());
            setCurrentPanel("search");
            frame.getContentPane().invalidate();
            frame.getContentPane().validate();
        } else if (string.equals("display")) {
            DisplayAll.updateList();
            frame.add(getDisplayPanel());
            setCurrentPanel("display");
            frame.getContentPane().invalidate();
            frame.getContentPane().validate();
        } else {
        }
    }
    
    /**
     * Exit program, ask user if they are sure
     */
    public static void quitProgram () {
        System.out.println("testing");
        if ((JOptionPane.showConfirmDialog(frame, "Are you sure you want to quit?",
                /* option pane title */ "Exit",
                /* option pane buttons */ JOptionPane.YES_NO_OPTION))
                /* if yes */ == JOptionPane.YES_OPTION) {
            System.exit(0);
        } else {
            
        }
    }
    
    /**
     * loadFile, ask user which file to load
     */
    public static void loadFile () {
        String fileName = JOptionPane.showInputDialog(frame, "Enter name of file to load:");
        System.out.println(fileName);
        try {
            if (fileName == null) {
                // nothing
            } else {
                Assignment3.getDayPlanner().openFile(fileName);
            }
        } catch (FileNotFoundException ex) {
            fileNotFoundError();
        }
    }
    
    /**
     * Display message if file was not found
     */
    public static void fileNotFoundError() {
        JOptionPane.showMessageDialog(frame, "Could not load file, please try again.");
    }
    
    /**
     * return the current panel
     * @return
     */
    public static String getCurrentPanel() {
        return currentPanel;
    }
    /**
     * set the current panel (which is a string)
     * @param string
     */
    public static void setCurrentPanel(String string) {
        currentPanel = string;
    }
    /**
     * return welcome panel
     * @return
     */
    public static WelcomePanel getWelcome() {
        return welcome;
    }
    /**
     * return add panel
     * @return
     */
    public static AddPanel getAddPanel() {
        return addPanel;
    }
    /**
     * return search panel
     * @return
     */
    public static SearchPanel getSearchPanel() {
        return searchPanel;
    }
    /**
     * return display panel
     * @return
     */
    public static DisplayAll getDisplayPanel() {
        return displayPanel;
    }
    
    /**
     * redraw the frame
     */
    public static void repaintFrame() {
        frame.repaint();
    }
    
    /**
     * repaint all of them
     */
    public static void repaintDisplayAll() {
        displayPanel.repaint();
    }
}