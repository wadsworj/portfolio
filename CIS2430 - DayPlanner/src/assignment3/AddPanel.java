/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */
package assignment3;

import java.awt.*;
import javax.swing.*;

/**
 * AddPanel manages the search display.
 * 
 * The JPanel is made up of three child JPanels, AddLeftPanel,
 * AddRightPanel, and AddBottomPanel
 * 
 * @author James Wadsworth
 */
public class AddPanel extends JPanel {
    
    private static final int LEFT_WIDTH = 360;
    private static final int LEFT_HEIGHT = 320;
    
    AddPanel() {
        
        setBackground(Color.WHITE);
        FlowLayout layout = new FlowLayout();
        GridBagConstraints c = new GridBagConstraints();
        setLayout(layout);

        AddRightPanel right = new AddRightPanel();
        AddLeftPanel left = new AddLeftPanel();
        AddBottomPanel bottom = new AddBottomPanel();

        // Set Sizes of JPanels
        left.setPreferredSize(new Dimension(LEFT_WIDTH, LEFT_HEIGHT));
        right.setPreferredSize(new Dimension(130, 250));
        bottom.setPreferredSize(new Dimension(510, 350));
        
        c.insets = new Insets(0,0,0,0);
        add(left, c);
        c.insets = new Insets(0,0,0,0);
        add(right, c);
        c.insets = new Insets(0,0,0,0);
        add(bottom, c);
        setVisible(true);
        
    }
}
