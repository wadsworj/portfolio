/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */
package assignment3;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * 
 * @author James Wadsworth
 */
public class SearchRightPanel extends JPanel implements MouseListener{
    // Right Panel
    SearchRightPanel() {
        setBackground(Color.WHITE);
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        ImageIcon resetButton = new ImageIcon("images/reset_button.png");
        
        JLabel reset = new JLabel(resetButton);
        reset.setText("Reset");
        reset.setHorizontalTextPosition(JLabel.CENTER);
        reset.addMouseListener(this);
        
        JLabel enter = new JLabel(resetButton);
        enter.setText("Enter");
        enter.setHorizontalTextPosition(JLabel.CENTER);
        enter.addMouseListener(this);
        
        reset.setAlignmentX(Component.CENTER_ALIGNMENT);
        enter.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        c.insets = new Insets(-100,0,0,0);
        add(reset, c);
        c.insets = new Insets(100,-72,0,0);
        add(enter, c);
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        JLabel label = (JLabel)e.getSource();
        if(label.getText() == "Reset") {
            System.out.println("Reset!!!");
            SearchLeftPanel.clear();
            SearchBottomPanel.setMessageText("");
        }
        if(label.getText() == "Enter") {
            SearchBottomPanel.setMessageText("");
            SearchLeftPanel.searchActivity();
        }
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mousePressed(MouseEvent e) {
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseExited(MouseEvent e) {
    }
}
