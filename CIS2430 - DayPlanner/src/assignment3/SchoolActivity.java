package assignment3;

/** SchoolActivity class
 *
 * @author Fei Song
 *
 * A class for representing and comparing school activities.
 *
 */

public class SchoolActivity extends Activity {
    SchoolActivity(String title, Time startingTime, Time endingTime, String comment) {
        super(title, startingTime, endingTime, "school", comment);
    }
}
