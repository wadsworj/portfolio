/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */

package assignment3;

import java.io.FileNotFoundException;

/**
 * The main class of the program
 * 
 * Prints a menu for the user and loads/saves a file
 * 
 * @author Fei Song/James Wadsworth
 */
public class Assignment3 {
    /**
     * @param args the command line arguments
     */
    
    private static DayPlanner planner;
    
    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
        // Check that args[1] exists
        try {
            if (args.length == 2) {
                planner = new DayPlanner();
                planner.openFile(args[2]);
            } else {
                planner = new DayPlanner();
                planner.openFile("file.txt");
            }
        } catch (FileNotFoundException e) {
            Interface.fileNotFoundError();
        }
        
        
        new Interface();
    }
    
    /**
     * 
     * @return
     */
    public static DayPlanner getDayPlanner() {
        return planner;
    }
    
    
    
    
    
}