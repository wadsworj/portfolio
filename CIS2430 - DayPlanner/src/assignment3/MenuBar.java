/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */
package assignment3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author lib_level
 */
public class MenuBar extends JMenuBar implements ActionListener {
    
    /**
     * 
     */
    public MenuBar() {
        JMenu commands = new JMenu("Commands");
        JMenuItem add = new JMenuItem("Add");
        JMenuItem search = new JMenuItem("Search");
        JMenuItem quit = new JMenuItem("Quit");
        
        JMenuItem display = new JMenuItem("Display All");
        JMenuItem loadFile = new JMenuItem("Load File");
        
        
        add.addActionListener(this);
        search.addActionListener(this);
        quit.addActionListener(this);
        display.addActionListener(this);
        loadFile.addActionListener(this);
        
        commands.add(add);
        commands.add(search);
        commands.add(display);
        commands.add(loadFile);
        commands.add(quit);
        
        add(commands);
    }

    /**
     * 
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String test = e.getActionCommand();
        
        if (test.equals("Add")) {
            
            Interface.removeCurrentPanel(Interface.getCurrentPanel());
            
            Interface.addPanel("add");
            Interface.repaintFrame();
            //re
        } else if (test.equals("Search")) {
            
            Interface.removeCurrentPanel(Interface.getCurrentPanel());
            Interface.addPanel("search");
            Interface.repaintFrame();
        } else if (test.equals("Display All")) {
            Interface.removeCurrentPanel(Interface.getCurrentPanel());
            Interface.addPanel("display");
            Interface.repaintDisplayAll();
            Interface.repaintFrame();
        } else if (test.equals("Load File")) {
            Interface.loadFile();
        } else if (test.equals("Quit")) {
            Interface.quitProgram();
        } else {
            // Nothing
        }
    }
}
