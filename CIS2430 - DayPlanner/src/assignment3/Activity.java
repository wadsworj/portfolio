package assignment3;

/** Activity class
 *
 * @author Fei Song
 *
 * A class for representing and comparing other activities.
 *
 */

public abstract class Activity {
	private String title;       // short description of the activity
	private Time startingTime;  // starting time of the activity
	private Time endingTime;    // ending time of the activity
	private String location;    // location of the activity
	private String comment;     // additional detail for the activity
	
	/**
	 * Constructor for a new activity with all the required fields
         * 
         * @param title Title of activity
         * @param startingTime Starting Time
         * @param comment Comment
         * @param endingTime Ending Time
         * @param location Location of event
         */
	public Activity(String title, Time startingTime, Time endingTime, String location, String comment) {
            this.title = title;
            this.startingTime = startingTime;
            this.endingTime = endingTime;
            this.location = location;
            this.comment = comment;
                
	}
	
	/**
	 * Check the validity for a potential activity
         * 
         * @param startingTime
         * @param endingTime 
         * @return  
         */
	public static boolean valid(Time startingTime, Time endingTime) {
		return (startingTime != null) && (endingTime != null);
	}

	/**
	 * Set a new value for title
         * 
         * @param title 
         */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Set a new value for starting time
         * 
         * @param startingTime 
         */
	public void setStartingTime(Time startingTime) {
		if (endingTime == null) {
			System.out.println("Invalid starting time");
			System.exit(0);
		} else
			this.startingTime = startingTime;
	}
	
	/**
	 * Set a new value for ending time
         * 
         * @param endingTime 
         */
	public void setEndingTime(Time endingTime) {
		if (startingTime == null) {
			System.out.println("Invalid ending time");
			System.exit(0);
		}
		this.endingTime = endingTime;
	}
	
	/**
	 * Set a new value for location
         * 
         * @param location 
         */
	public void setLocation(String location) {
		this.location = location;
	}
	
	/**
	 * Set a new value for comment
         * 
         * @param comment 
         */
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	/**
	 * Get the value of title
         * 
         * @return title
         */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Get the value of starting time
         * 
         * @return startingTime
         */
	public Time getStartingTime() {
		return startingTime;
	}
	
	/**
	 * Get the value of ending time
         * 
         * @return endingTime
         */
	public Time getEndingTime() {
		return endingTime;
	}
	
	/**
	 * Get the value of location
         * 
         * @return location
         */
	public String getLocation() {
		return location;
	}
	
	/**
	 * Get the value of comment
         * 
         * @return comment
         */
	public String getComment() {
		return comment;
	}
	
	/**
	 * Check for equality of two other activities
         * 
         * @param other 
         * @return true or false
         */
	public boolean equals(Activity other) {
		if (other == null)
			return false;
		else 
			return title.equals(other.title) &&
			       startingTime.equals(other.startingTime) &&
			       endingTime.equals(other.endingTime) &&
			       location.equals(other.location) &&
			       comment.equals(other.comment);
	}
	
	/**
	 * Show the content of an other activity in a string
         * 
         * @return String representation of activity
         */
	public String toString() {
		return title + "\n: " + startingTime + " to " + endingTime + ",\n " + location + ",\n " + comment;	
	}
	
        /**
         * 
         * @param args
         */
        public static void main(String[] args) {
		Time startingTime = new Time(2009, 10, 22, 12, 30);
		Time endingTime = new Time(2009, 10, 22, 13, 20);
		OtherActivity activity = new OtherActivity("Lunch", startingTime, endingTime, "Time Horton", "");
		System.out.println("Activity: " + activity);
	}
}
