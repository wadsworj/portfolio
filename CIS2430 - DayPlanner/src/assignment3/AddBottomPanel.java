/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */
package assignment3;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;

/**
 * AddBottomPanel
 * Creates a JPanel to be placed at the bottom of the ADD panel.
 * This panel displays all error messages in a JTextArea
 * @author James Wadsworth
 */
public class AddBottomPanel extends JPanel implements MouseListener {
    
    private static JTextArea messagesOutput;
    
    AddBottomPanel() {
        setBackground(Color.WHITE);
        setLayout(new FlowLayout(FlowLayout.LEFT));
        JTextArea messages = new JTextArea();
        messages.setText("Messages:\t\t\t\t\t\t");
        
        messagesOutput = new JTextArea();
        messagesOutput.setPreferredSize(new Dimension(480, 145));
        JScrollPane scroll = new JScrollPane(messagesOutput);
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        
        add(messages);
        add(scroll);
    }
    /**
     * 
     * @param string
     */
    public static void setMessageText(String string) {
        messagesOutput.setText(string);
        
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mousePressed(MouseEvent e) {
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseExited(MouseEvent e) {
    }
}
