/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */
package assignment3;

import java.awt.*;
import javax.swing.*;

/**
 * SearchBottomPanel
 * Creates a JPanel to be placed at the bottom of the SEARCH panel.
 * This panel displays all search results in a JTextArea
 * @author James Wadsworth
 */
public class SearchBottomPanel extends JPanel {
    
    private static JTextArea messagesOutput;
    
    SearchBottomPanel() {
        // Set basic elements
        setBackground(Color.WHITE);
        setLayout(new FlowLayout(FlowLayout.LEFT));
        
        // Create header
        JTextArea messages = new JTextArea();
        messages.setText("Search Results:\t\t\t\t\t\t");
        
        messagesOutput = new JTextArea();
        messagesOutput.setWrapStyleWord(true);
        
        JScrollPane scroll = new JScrollPane(messagesOutput);
        
        scroll.setPreferredSize(new Dimension(480, 245));
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        
        // Add elements to JPanel
        add(messages);
        add(scroll);
    }

    /**
     * Set the text in the results panel
     * @param string text to set
     */
    public static void setMessageText(String string) {
        messagesOutput.setText(string);
    }
    
    /**
     * Add text to the results panel without clearing it first.
     * Saves what was already in results box.
     * @param string text to add
     */
    public static void addMessageText(String string) {
        messagesOutput.setText(messagesOutput.getText() + string);
    }
}