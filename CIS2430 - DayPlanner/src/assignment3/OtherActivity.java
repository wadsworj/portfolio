package assignment3;

/** OtherActivity class
 *
 * @author Fei Song
 *
 * A class for representing and comparing other activities.
 *
 */

public class OtherActivity extends Activity {
    
    OtherActivity(String title, Time startingTime, Time endingTime, String location, String comment) {
        super(title, startingTime, endingTime, location, comment);
    
    }
}
