/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */

package assignment3;

/** HomeActivity class
 *
 * @author Fei Song
 *
 * A class for representing and comparing home activities.
 *
 */

public class HomeActivity extends Activity {
    /*
     * Constructor 
     */
    HomeActivity(String title, Time startingTime, Time endingTime, String comment) {
        super(title, startingTime, endingTime, "home", comment);
    
    }
}
