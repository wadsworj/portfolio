/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */
package assignment3;

/**
 * A custom exception to be thrown.
 * @author James Wadsworth
 */
public class InvalidTimeException extends Exception {
    /**
     * 
     * @param message
     */
    public InvalidTimeException(String message) {
        super(message);
        
    }
}