/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */

package assignment3;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Class for file related methods
 * @author James Wadsworth
 */
public class File {
    /**
     *  A function to save the current list to a file
     * @param list
     * @param file
     */
    public static void saveFile(ArrayList<Activity> list, String file) {
        PrintWriter outputFile = null;
        try {
            outputFile = new PrintWriter(new FileOutputStream(file));
        } catch (FileNotFoundException e) {
            System.out.println("Error opening file");
            System.exit(0);
        }
        
        for (int i = 0; i < list.size(); i++) {
            outputFile.print("type = \"");
            if (list.get(i) instanceof OtherActivity) {
                outputFile.print("other\"\n");
            }
            if (list.get(i) instanceof HomeActivity) {
                outputFile.print("home\"\n");
            }
            if (list.get(i) instanceof SchoolActivity) {
                outputFile.print("school\"\n");
            }
            outputFile.print("title = \"" + list.get(i).getTitle() + "\"\n");
            outputFile.print("start = \"" + list.get(i).getStartingTime().toStringPrint() + "\"\n");
            outputFile.print("end = \"" + list.get(i).getEndingTime().toStringPrint() + "\"\n");
            if (list.get(i) instanceof OtherActivity) {
                outputFile.print("location = \"" + list.get(i).getLocation() + "\"\n");
            }
            outputFile.print("comment = \"" + list.get(i).getComment() + "\"");
            
            if(i < (list.size() - 1) ) {
                outputFile.println("\n");
            }
        }
        
        outputFile.close();
    }
    
    
}
