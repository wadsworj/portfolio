package assignment3;

/** Time class 
 * 
 * @author Fei Song
 *
 * A class for representing and comparing different times.
 *
 */
public class Time {
	private int year;
	private int month;
	private int day;
	private int hour;
	private int minute;

	/**
	 * Create a time object with all the requirement elements
         * 
         * @param year 
         * @param day
         * @param month
         * @param hour
         * @param minute  
         */
	public Time(int year, int month, int day, int hour, int minute) {
		if (timeOK(year, month, day, hour, minute)) {
			this.year = year;
			this.month = month;
			this.day = day;
			this.hour = hour;
			this.minute = minute;
		} else {
			System.out.println("Fatal Error for Time");
			System.exit(0);
		}
	}
        
        // CODE WRITTEN BY FEI SONG, MODIFIED TO BE A CONSTRUCTOR
        /**
         * 
         * @param line
         * @throws InvalidTimeException
         */
        public Time(String line) throws InvalidTimeException {
            String[] tokens = line.split("[ ,\n]+");
            if (tokens.length != 5)
                    throw new InvalidTimeException("Time not formatted right."
                            + "\nPlease use either a spaces or commas to seperate elements");
                    //return null;
            for (int i = 0; i < 5; i++ ) {
                if (!tokens[i].matches("[-+]?[0-9]+")) {
                    //return null;
                }
            }
            year = Integer.parseInt(tokens[0]);
            month = Integer.parseInt(tokens[1]);
            day = Integer.parseInt(tokens[2]);
            hour = Integer.parseInt(tokens[3]);
            minute = Integer.parseInt(tokens[4]);
            
            if (timeOK(year, month, day, hour, minute)) {
                /* don't think we need any 
                this.year = year;
		this.month = month;
                this.day = day;
		this.hour = hour;
		this.minute = minute;
                */
            }
            else {
                    //return null;
                throw new InvalidTimeException("Time input out of range.");
            }
    }
	
	/**
	 * Create a time object with no arguments
	 */
	public Time() {
		year = 0;
		month = 0;
		day = 0;
		hour = 0;
		minute = 0;
	}
	
	/**
	 * Set a new value for year
         * 
         * @param year 
         */
	public void setYear(int year) {
		if (year >= 0)
			this.year = year;
		else {
			System.out.println("Invalid value for year: " + year);
			System.exit(0);
		}
	}
	
	/**
	 * Set a new value for month
         * 
         * @param month 
         */
	public void setMonth(int month)	{
		if (month >= 0)
			this.month = month;
		else {
			System.out.println("Invalid value for month: " + month);
			System.exit(0);
		}
	}
	
	/**
	 * Set a new value for day
         * 
         * @param day 
         */
	public void setDay(int day) {
		if (day >= 0) 
			this.day = day;
		else {
			System.out.println("Invalid value for day: " + day);
			System.exit(0);
		}
	}
	
	/**
	 * Set a new value for hour
         * 
         * @param hour 
         */
	public void setHour(int hour) {
		if (hour >= 0)
			this.hour = hour;
		else {
			System.out.println("Invalid value for hour: " + hour);
			System.exit(0);
		}
	}
	
	/**
	 * Set a new value for minute
         * 
         * @param minute 
         */
	public void setMinute(int minute) {
		if (minute >= 0)
			this.minute = minute;
		else {
			System.out.println("Invalid value for minute: " + minute);
			System.exit(0);
		}
	}
	
	/**
	 * Check for equality with a given time object
         * 
         * @param other
         * @return  
         */
	public boolean equals(Time other) {
		if (other == null)
			return false;
		else 
			return year == other.year &&
				month == other.month &&
				day == other.day &&
				hour == other.hour &&
				minute == other.minute;
	}
	
	/**
	 * Compare two time objects for ordering
         * 
         * @param other
         * @return  
         */
	public int compareTo(Time other) {
		if (year < other.year)
			return -1;
		else if (year > other.year)
			return 1;
		else if (month < other.month)
			return -1;
		else if (month > other.month)
			return 1;
		else if (day < other.day)
			return -1;
		else if (day > other.day)
			return 1;
		else if (hour < other.hour)
			return -1;
		else if (hour > other.hour)
			return 1;
		else if (minute < other.minute)
			return -1;
		else if (minute > other.minute)
			return 1;
		else
			return 0;
	}
	
	/**
	 * Get the value for year
         * 
         * @return 
         */
	public int getYear() {
		return year;
	}
	
	/**
	 * Get the value for month
         * 
         * @return 
         */
	public int getMonth() {
		return month;
	}
	
	/**
	 * Get the value for day
         * 
         * @return 
         */
	public int getDay() {
		return day;
	}
	
	/**
	 * Get the value for hour
         * 
         * @return 
         */
	public int getHour() {
		return hour;
	}
	
	/**
	 * Get the value for minute
         * 
         * @return 
         */
	public int getMinute() {
		return minute;
	}
	
	/**
	 * Show the content of a time object in a string
         * 
         * @return 
         */
	public String toString() {
		return hour + ":" + minute + ", " + month + "/" + day + "/" + year;
	} 
        
        /**
         * 
         * @return
         */
        public String toStringPrint() {
		return year + ", " + month + ", " + day + ", " + hour + ", " + minute;
	} 
	
	/**
	 * Validate all the fields for a time object
         * 
         * @param year 
         * @param day
         * @param month
         * @param hour 
         * @param minute 
         * @return  
         */
	static public boolean timeOK(int year, int month, int day, int hour, int minute) {
		return (year >= 0) && ((month > 0) && (month <= 12))&& ((day > 0) && (day <= 31))&&
		       ((hour >= 0) && (hour <= 24)) && ((minute >= 0) && (minute < 60));
	}
	
        /**
         * 
         * @param args
         */
        public static void main(String[] args) {
		Time startingTime = null;
		if (Time.timeOK(2014, 10, 22, 12, 30)) 
			startingTime = new Time(2014, 10, 22, 12, 30);
	
		Time endingTime = null;
		if (Time.timeOK(2014, 10, 22, 13, 20)) 
			endingTime = new Time(2014, 10, 22, 13, 20);
		
		if (startingTime != null && endingTime != null) {
			int result = startingTime.compareTo(endingTime);
			if (result < 0)
				System.out.println(startingTime + " precedes " + endingTime);
			else if (result > 0)
				System.out.println(startingTime + " follows " + endingTime);
			else
				System.out.println(startingTime + " equals " + endingTime);		
		}
	}
        
}
