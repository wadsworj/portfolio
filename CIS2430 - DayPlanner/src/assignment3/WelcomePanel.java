/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */
package assignment3;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * Initial window user will see. Welcome the user and
 * give initial instructions.
 * 
 * @author James Wadsworth
 */
public class WelcomePanel extends JPanel implements MouseListener {
    
    WelcomePanel() {
        
        ImageIcon resetButton = new ImageIcon("images/reset_button.png");
        
        setBackground(Color.WHITE);
        
        JTextArea welcome = new JTextArea();
        
        welcome.setEditable(false);
        welcome.setPreferredSize(new Dimension(480, 205));
        Font font = new Font("Verdana", Font.PLAIN, 14);
        welcome.setFont(font);
        
        welcome.setText("\n\n\nWelcome to my Day Planner."
                + "\n\n\nChoose a command from the \"Commands\" menu"
                + " above for adding\nan activity, searching activities, or"
                + " quitting the program\n\n"
                + "You can also make a choice from below.");
        
        JPanel buttons = new JPanel();
        buttons.setPreferredSize(new Dimension(480, 145));
        buttons.setBackground(Color.WHITE);
        JLabel add = new JLabel(resetButton);
        add.setText("Add");
        add.setHorizontalTextPosition(JLabel.CENTER);
        add.addMouseListener(this);
        
        JLabel search = new JLabel(resetButton);
        search.setText("Search");
        search.setHorizontalTextPosition(JLabel.CENTER);
        search.addMouseListener(this);
        
        JLabel quit = new JLabel(resetButton);
        quit.setText("Quit");
        quit.setHorizontalTextPosition(JLabel.CENTER);
        quit.addMouseListener(this);
        
        buttons.add(add);
        buttons.add(search);
        buttons.add(quit);

        add(welcome);
        add(buttons);
        setVisible(true);
    }

    /**
     * MouseClicked event. Do something when user clicks
     * button.
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        JLabel label = (JLabel)e.getSource();
        
        if (label.getText().equals("Add")) {
            Interface.removeCurrentPanel(Interface.getCurrentPanel());
            Interface.addPanel("add");
            Interface.repaintFrame();
            //re
        } else if (label.getText().equals("Search")) {
            Interface.removeCurrentPanel(Interface.getCurrentPanel());
            Interface.addPanel("search");
            Interface.repaintFrame();
        } else if (label.getText().equals("Quit")) {
            System.exit(0);
        } else {
            // Nothing
        }
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mousePressed(MouseEvent e) {
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseExited(MouseEvent e) {
    }
}
