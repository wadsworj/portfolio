/*
 * Author: James Wadsworth
 * Student ID: 0813414
 * Date: November 28th, 2014
 */
package assignment3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import javax.naming.SizeLimitExceededException;

/**
 * AddLeftPanel creates the JPanel on the left of the ADD panel.
 * This panel manages the form for adding an activity.
 * 
 * Each section of the form is added by using a JPanel with text
 * and elements added to each JPanel.
 * 
 * @author James Wadsworth
 */
public class AddLeftPanel extends JPanel implements MouseListener, ActionListener {
    
    private static final int LEFT_WIDTH = 360;
    private static final int LEFT_HEIGHT = 320;
    
    private static JTextField titleBox;
    private static JTextField startingBox;
    private static JTextField endingBox;
    private static JTextField locationBox;
    private static JTextField commentBox;
    private static JPanel location;
    
    private static JComboBox typeBox;
    private static final String[] typeStrings = {"School", "Home", "Other"};
    
    AddLeftPanel() {
        setBackground(Color.WHITE);
        setLayout(new FlowLayout());
        
        
        JTextArea text1 = new JTextArea();
        JTextArea text2 = new JTextArea();
        JTextArea text3 = new JTextArea();
        JTextArea text4 = new JTextArea();
        JTextArea text5 = new JTextArea();
        JTextArea text6 = new JTextArea();
        JTextArea text7 = new JTextArea();
        JTextArea newLine = new JTextArea();
        
        text1.setEditable(false);
        text2.setEditable(false);
        text3.setEditable(false);
        text4.setEditable(false);
        text5.setEditable(false);
        text6.setEditable(false);
        text7.setEditable(false);
        newLine.setEditable(false);
        
        Font font = new Font("Verdana", Font.PLAIN, 12);
        text1.setFont(font);
        text2.setFont(font);
        text3.setFont(font);
        text4.setFont(font);
        text5.setFont(font);
        text6.setFont(font);
        text7.setFont(font);
        
        
        text1.setText("Adding an activity:");
        text1.setPreferredSize(new Dimension( 300, 24 ));
        text2.setText("     Type: ");
        text3.setText("     Title: ");
        text4.setText(" Starting time (year, month, day, hour, minute):");
        text5.setText(" Ending time (year, month, day, hour, minute): ");
        text6.setText("  Location");
        text7.setText("  Comment: ");
       
        
        // JComboBoxes
        typeBox = new JComboBox(typeStrings);
        typeBox.setSelectedIndex(0);
        typeBox.setPreferredSize(new Dimension( 200, 24 ));
        typeBox.addActionListener(this);
                
        // JTextFields
        titleBox  = new JTextField();
        titleBox.setText("");
        titleBox.setPreferredSize(new Dimension( 250, 24 ));
        
        startingBox  = new JTextField();
        startingBox.setText("");
        startingBox.setPreferredSize(new Dimension( 250, 24 ));
        
        endingBox  = new JTextField();
        endingBox.setText("");
        endingBox.setPreferredSize(new Dimension( 250, 24 ));
        
        locationBox  = new JTextField();
        locationBox.setText("");
        locationBox.setPreferredSize(new Dimension( 250, 24 ));
        
        commentBox  = new JTextField();
        commentBox.setText("");
        commentBox.setPreferredSize(new Dimension( 250, 24 ));
        
        // JPanel Instances
        JPanel addingActivity = new JPanel();
        addingActivity.setLayout(new FlowLayout(FlowLayout.LEFT));
        addingActivity.setPreferredSize(new Dimension(LEFT_WIDTH, 25));
        addingActivity.setBackground(Color.WHITE);
        
        JPanel type = new JPanel();
        type.setPreferredSize(new Dimension(LEFT_WIDTH, 35));
        type.setLayout(new FlowLayout(FlowLayout.LEFT));
        type.setBackground(Color.WHITE);
        
        JPanel title = new JPanel();
        title.setPreferredSize(new Dimension(LEFT_WIDTH, 35));
        title.setLayout(new FlowLayout(FlowLayout.LEFT));
        title.setBackground(Color.WHITE);
        
        JPanel time = new JPanel();
        time.setPreferredSize(new Dimension(LEFT_WIDTH, 105));
        time.setLayout(new FlowLayout(FlowLayout.LEFT));
        time.setBackground(Color.WHITE);
        
        location = new JPanel();
        location.setPreferredSize(new Dimension(LEFT_WIDTH, 35));
        location.setLayout(new FlowLayout(FlowLayout.LEFT));
        location.setBackground(Color.WHITE);
        
        JPanel comment = new JPanel();
        comment.setPreferredSize(new Dimension(LEFT_WIDTH, 35));
        comment.setLayout(new FlowLayout(FlowLayout.LEFT));
        comment.setBackground(Color.WHITE);
        
        
        addingActivity.add(text1);

        type.add(text2);
        type.add(typeBox);
        
        title.add(text3);
        title.add(titleBox);
        
        time.add(text4);
        time.add(startingBox);
        time.add(text5);
        time.add(endingBox);
        
        location.add(text6);
        location.add(locationBox);
        
        comment.add(text7);
        comment.add(commentBox);
        
        add(addingActivity);
        add(type);
        add(title);
        add(time);
        
        add(comment);
        
    }
    
    /**
     * 
     */
    public static void clear() {
        titleBox.setText("");
        startingBox.setText("");
        endingBox.setText("");
        locationBox.setText("");
        commentBox.setText("");
    }
    
    private static void clearTime() {
        startingBox.setText("");
        endingBox.setText("");
    }
    
    /**
     * 
     */
    public static void enterActivity(){
        /* exception handling for creating a new activity */
        try {
            if(titleBox.getText().equals("")) {
                throw new IOException("empty title");
            }
            
            Time startingTime = new Time(startingBox.getText());
            Time endingTime = new Time(endingBox.getText());
            String type = typeStrings[typeBox.getSelectedIndex()];
            
            if (type.equals("Other")) {
                OtherActivity activity = new OtherActivity(titleBox.getText(), startingTime,
                                                endingTime, locationBox.getText(), commentBox.getText());
                DayPlanner.addActivity(activity);
            } else if (type.equals("School")) {
                SchoolActivity activity = new SchoolActivity(titleBox.getText(), startingTime,
                                                endingTime, commentBox.getText());
                DayPlanner.addActivity(activity);
            } else if (type.equals("Home")) {
                HomeActivity activity = new HomeActivity(titleBox.getText(), startingTime,
                                                endingTime, commentBox.getText());
                DayPlanner.addActivity(activity);
            } else {
            }
            // Print message that activity was added succesfully
            AddBottomPanel.setMessageText("Activity \"" + titleBox.getText() + "\" added succesfully.");
            clear();
        } catch (IOException io) {
            AddBottomPanel.setMessageText("No title given! Please try again");
        } catch (InvalidTimeException e) {
            AddBottomPanel.setMessageText(e.getMessage() +
                    "\nInvalid Time Format! Please try again");
            clearTime();
        } catch (SizeLimitExceededException s) {
            AddBottomPanel.setMessageText("No more room for activites!");
        } finally {
            // clean up
        }
        
        
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mousePressed(MouseEvent e) {
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseExited(MouseEvent e) {
    }

    /**
     * 
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        JComboBox box = (JComboBox)e.getSource();
        String string = typeStrings[typeBox.getSelectedIndex()];
        System.out.println(string);
        if (string.equals("Other")) {
            
            add(location);
            revalidate();
            repaint();
        } else {
            remove(location);
            revalidate();
            repaint();
        }
    }
}
